<?php
    //Pages controller
    Router::connect('/',				array('controller' => 'pages',	'action' => 'homepage'));
	Router::connect('/about',		array('controller' => 'pages',	'action' => 'view',				'about'));
	Router::connect('/shortcuts',	array('controller' => 'pages',	'action' => 'view',				'shortcuts'));
	
	//Browser controller
	Router::connect('/:type',		array('controller' => 'browser',	'action' => 'sources'),		array('pass' => array('type'), 'type' => 'audio|image|video'));
    Router::connect('/:type/:id/*',	array('controller' => 'browser',	'action' => 'browse'),		array('pass' => array('type', 'id'), 'type' => 'audio|image|video', 'id' => '[0-9]+'));
    Router::connect('/download/*',	array('controller' => 'browser',	'action' => 'download'));
    Router::connect('/play/*',		array('controller' => 'browser',	'action' => 'play'));
    Router::connect('/search/*',		array('controller' => 'browser',	'action' => 'search'));
    
	//Playlists controller
    Router::connect('/playdir/*',			array('controller' => 'playlists',	'action' => 'play_as_dir'));
	Router::connect('/playlists/:type',		array('controller' => 'playlists',	'action' => 'index'),			array('pass' => array('type'), 'type' => 'audio|video'));
	Router::connect('/playlists/:type/:id',	array('controller' => 'playlists',	'action' => 'play'),			array('pass' => array('type', 'id'), 'type' => 'audio|video', 'id' => '[0-9]+'));
	
	//Tools controller
	Router::connect('/cache',		array('controller' => 'tools',	'action' => 'cache'));
	Router::connect('/checkup',		array('controller' => 'tools',	'action' => 'checkup'));
	Router::connect('/config',		array('controller' => 'tools',	'action' => 'configuration'));
	Router::connect('/nosources',	array('controller' => 'tools',	'action' => 'no_sources'));
	Router::connect('/sources',		array('controller' => 'tools',	'action' => 'sources'));
	Router::connect('/sources/add',	array('controller' => 'tools',	'action' => 'add_source'));
	Router::connect('/updates',		array('controller' => 'tools',	'action' => 'updates'));

    Router::parseExtensions('json');
	
	//Load all plugin routes
    CakePlugin::routes();

	//Load the CakePHP default routes
    require CAKE.'Config'.DS.'routes.php';
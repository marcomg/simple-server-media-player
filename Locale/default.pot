# LANGUAGE translation of SSMP
# Copyright 2014 Mirko Pagliai <mirko.pagliai@gmail.com
#
#, fuzzy
msgid ""
msgstr ""
"POT-Creation-Date: 2014-02-26 17:00+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/AppController.php:234
msgid "Invalid request, no media type specified"
msgstr ""

#: Controller/AppController.php:348
msgid "Your IP address is not allowed"
msgstr ""

#: Controller/BrowserController.php:172
msgid "You can perform a search every %s seconds"
msgstr ""

#: Controller/PlaylistsController.php:60
msgid "In order to use playlists, you have to use jPlayer as media player"
msgstr ""

#: Controller/PlaylistsController.php:121
msgid "The playlist has been created"
msgstr ""

#: Controller/PlaylistsController.php:125
msgid "The playlist has not been created"
msgstr ""

#: Controller/PlaylistsController.php:128
msgid "A playlist with this name already exists"
msgstr ""

#: Controller/PlaylistsController.php:147
msgid "The playlist has been deleted"
msgstr ""

#: Controller/PlaylistsController.php:149
msgid "The playlist has not been deleted"
msgstr ""

#: Controller/PlaylistsController.php:164
msgid "This action should be called by Ajax"
msgstr ""

#: Controller/ToolsController.php:67
msgid "You cannot add new sources, because the file <em>%s</em> is not writable"
msgstr ""

#: Controller/ToolsController.php:88
msgid "The new source has been added"
msgstr ""

#: Controller/ToolsController.php:140
msgid "The cache has been cleared"
msgstr ""

#: Controller/ToolsController.php:142
msgid "The cache has not been cleared"
msgstr ""

#: Controller/ToolsController.php:155
msgid "Thumbnails have been deleted"
msgstr ""

#: Controller/ToolsController.php:157
msgid "Thumbnails have not been deleted"
msgstr ""

#: Controller/ToolsController.php:169
msgid "You cannot edit configuration, because the file <em>%s</em> is not writable"
msgstr ""

#: Controller/ToolsController.php:188
msgid "The configuration has been saved"
msgstr ""

#: Controller/ToolsController.php:215
msgid "You cannot delete sources, because the file <em>%s</em> is not writable"
msgstr ""

#: Controller/ToolsController.php:221
msgid "You cannot delete this source"
msgstr ""

#: Controller/ToolsController.php:228
msgid "The source has been deleted"
msgstr ""

#: View/Browser/browse_audio.ctp:29
#: View/Browser/play_audio.ctp:29
#: View/Browser/sources.ctp:29
#: View/Elements/search.ctp:40
#: View/Elements/topbar.ctp:44;49
#: View/Pages/homepage.ctp:32
#: View/Tools/add_source.ctp:36
msgid "Audio"
msgstr ""

#: View/Browser/browse_audio.ctp:37
#: View/Browser/browse_video.ctp:37
msgid "Play folder"
msgstr ""

#: View/Browser/browse_audio.ctp:38
#: View/Browser/browse_video.ctp:38
msgid "Add to playlist"
msgstr ""

#: View/Browser/browse_image.ctp:29
#: View/Browser/play_image.ctp:29
#: View/Browser/sources.ctp:29
#: View/Elements/search.ctp:40
#: View/Elements/topbar.ctp:53;55
#: View/Pages/homepage.ctp:37
#: View/Tools/add_source.ctp:36
msgid "Images"
msgstr ""

#: View/Browser/browse_image.ctp:36
msgid "Display as list"
msgstr ""

#: View/Browser/browse_image.ctp:38
msgid "Display as thumbs"
msgstr ""

#: View/Browser/browse_video.ctp:29
#: View/Browser/play_video.ctp:29
#: View/Browser/sources.ctp:29
#: View/Elements/topbar.ctp:59;64
#: View/Pages/homepage.ctp:42
msgid "Video"
msgstr ""

#: View/Browser/play_audio.ctp:46
msgid "Your browser doesn't support the HTML5 audio tag"
msgstr ""

#: View/Browser/play_audio.ctp:52
#: View/Browser/play_image.ctp:38
#: View/Browser/play_video.ctp:48
msgid "File info"
msgstr ""

#: View/Browser/play_audio.ctp:54
msgid "Id3 Audio"
msgstr ""

#: View/Browser/play_audio.ctp:63
#: View/Browser/play_image.ctp:49
#: View/Browser/play_video.ctp:61
msgid "Name"
msgstr ""

#: View/Browser/play_audio.ctp:65
#: View/Browser/play_image.ctp:51
#: View/Browser/play_video.ctp:63
msgid "Directory"
msgstr ""

#: View/Browser/play_audio.ctp:67
#: View/Browser/play_image.ctp:53
#: View/Browser/play_video.ctp:65
msgid "Size"
msgstr ""

#: View/Browser/play_audio.ctp:69
#: View/Browser/play_image.ctp:55
#: View/Browser/play_video.ctp:67
msgid "Type"
msgstr ""

#: View/Browser/play_audio.ctp:79
msgid "Title"
msgstr ""

#: View/Browser/play_audio.ctp:83
msgid "Album"
msgstr ""

#: View/Browser/play_audio.ctp:87
msgid "Artist"
msgstr ""

#: View/Browser/play_audio.ctp:91
msgid "Track"
msgstr ""

#: View/Browser/play_audio.ctp:95
msgid "Disk"
msgstr ""

#: View/Browser/play_audio.ctp:99
msgid "Year"
msgstr ""

#: View/Browser/play_audio.ctp:103
#: View/Browser/play_video.ctp:77;121
msgid "Format"
msgstr ""

#: View/Browser/play_audio.ctp:107
#: View/Browser/play_video.ctp:81
msgid "Codec"
msgstr ""

#: View/Browser/play_audio.ctp:111;135
msgid "Encoder"
msgstr ""

#: View/Browser/play_audio.ctp:115
#: View/Browser/play_video.ctp:85
msgid "Sample rate"
msgstr ""

#: View/Browser/play_audio.ctp:119
#: View/Browser/play_video.ctp:97
msgid "Channels"
msgstr ""

#: View/Browser/play_audio.ctp:123
#: View/Browser/play_video.ctp:101
msgid "Channel mode"
msgstr ""

#: View/Browser/play_audio.ctp:127
#: View/Browser/play_video.ctp:89;129
msgid "Bitrate"
msgstr ""

#: View/Browser/play_audio.ctp:131
#: View/Browser/play_video.ctp:109;137
msgid "Playtime"
msgstr ""

#: View/Browser/play_audio.ctp:144
#: View/Browser/play_image.ctp:77
#: View/Browser/play_video.ctp:146
msgid "Download"
msgstr ""

#: View/Browser/play_image.ctp:40
msgid "Exif data"
msgstr ""

#: View/Browser/play_video.ctp:43
msgid "Your browser doesn't support the HTML5 video tag"
msgstr ""

#: View/Browser/play_video.ctp:50;52
msgid "Id3 Video"
msgstr ""

#: View/Browser/play_video.ctp:93
#: View/Tools/configuration.ctp:34
msgid "Language"
msgstr ""

#: View/Browser/play_video.ctp:105
msgid "Bit depth"
msgstr ""

#: View/Browser/play_video.ctp:125
msgid "Resolution"
msgstr ""

#: View/Browser/play_video.ctp:133
msgid "Frame rate"
msgstr ""

#: View/Browser/search.ctp:29
#: View/Elements/search.ctp:43
#: View/Elements/topbar.ctp:67
#: View/Pages/homepage.ctp:46
msgid "Search"
msgstr ""

#: View/Browser/search.ctp:29
msgid "Search media files"
msgstr ""

#: View/Browser/search.ctp:36
msgid "Results for this source"
msgstr ""

#: View/Browser/search.ctp:50
msgid "Search was performed in %s seconds"
msgstr ""

#: View/Browser/sources.ctp:31
msgid "Select source"
msgstr ""

#: View/Elements/add_to_playlist.ctp:32
msgid "create"
msgstr ""

#: View/Elements/add_to_playlist.ctp:38
msgid "Select a playlist"
msgstr ""

#: View/Elements/add_to_playlist.ctp:43
msgid "Otherwise, you can %s a new playlist"
msgstr ""

#: View/Elements/add_to_playlist.ctp:46
msgid "In order to add media files to a playlist, you need to %s a playlist"
msgstr ""

#: View/Elements/jplayer_audio.ctp:84
#: View/Elements/jplayer_audio_playlist.ctp:95
#: View/Elements/jplayer_video.ctp:91
#: View/Elements/jplayer_video_playlist.ctp:104
msgid "Your browser can't play this media file, probably because there aren't necessary codecs or because it doesn't support HTML5 audio/video elements"
msgstr ""

#: View/Elements/search.ctp:33
msgid "Pattern"
msgstr ""

#: View/Elements/search.ctp:34
msgid "What are you searching for?"
msgstr ""

#: View/Elements/search.ctp:35
msgid "The search is case-insensitive and you don't have to specify the file extension, even if you're using a regex"
msgstr ""

#: View/Elements/search.ctp:38
msgid "I'm searching for..."
msgstr ""

#: View/Elements/search.ctp:39
msgid "Media type"
msgstr ""

#: View/Elements/search.ctp:40
#: View/Tools/add_source.ctp:36
msgid "Videos"
msgstr ""

#: View/Elements/search.ctp:42
msgid "Regex"
msgstr ""

#: View/Elements/topbar.ctp:31
msgid "Toggle navigation"
msgstr ""

#: View/Elements/topbar.ctp:41
msgid "Home"
msgstr ""

#: View/Elements/topbar.ctp:45;60
#: View/Pages/about.ctp:45
#: View/Tools/sources.ctp:29
msgid "Sources"
msgstr ""

#: View/Elements/topbar.ctp:46;61
#: View/Playlists/index.ctp:31
msgid "Playlists"
msgstr ""

#: View/Elements/topbar.ctp:71
msgid "Tools"
msgstr ""

#: View/Elements/topbar.ctp:73
#: View/Pages/about.ctp:29
msgid "About"
msgstr ""

#: View/Elements/topbar.ctp:74
#: View/Pages/shortcuts.ctp:29
msgid "Shortcuts"
msgstr ""

#: View/Elements/topbar.ctp:75
#: View/Tools/configuration.ctp:28
msgid "Configuration"
msgstr ""

#: View/Elements/topbar.ctp:76
#: View/Tools/sources.ctp:29
msgid "Manage sources"
msgstr ""

#: View/Elements/topbar.ctp:77
msgid "System check up"
msgstr ""

#: View/Elements/topbar.ctp:78
#: View/Tools/cache.ctp:29;31
msgid "Cache"
msgstr ""

#: View/Elements/topbar.ctp:78
#: View/Tools/cache.ctp:29;40
msgid "Thumbs"
msgstr ""

#: View/Elements/topbar.ctp:79
msgid "Check for updates"
msgstr ""

#: View/Pages/about.ctp:29
msgid "About %s"
msgstr ""

#: View/Pages/about.ctp:31
msgid "%s (%s) is a (simple) web application for playing media contents (such as audio, images and videos) located on a server."
msgstr ""

#: View/Pages/about.ctp:32
msgid "It was developed by %s for %s and is written in PHP and Javascript, using the %s framework."
msgstr ""

#: View/Pages/about.ctp:37
msgid "Support"
msgstr ""

#: View/Pages/about.ctp:38
msgid "If you need support for %s, please visit %s and look at the %s."
msgstr ""

#: View/Pages/about.ctp:39
#: View/Tools/updates.ctp:40
msgid "our website"
msgstr ""

#: View/Pages/about.ctp:40
msgid "support page"
msgstr ""

#: View/Pages/about.ctp:41
msgid "Instead, if you have issues playing an audio or video file, see the %s and make sure your browser has support for HTML5 and codecs to play the file. In fact, only some browsers can play all file formats supported by %s."
msgstr ""

#: View/Pages/about.ctp:42
msgid "compatibility table"
msgstr ""

#: View/Pages/about.ctp:46
msgid "%s sources are freely available from %s."
msgstr ""

#: View/Pages/about.ctp:47
msgid "our git repository"
msgstr ""

#: View/Pages/about.ctp:49
msgid "Changelog"
msgstr ""

#: View/Pages/about.ctp:50
msgid "You can find the changelog %s."
msgstr ""

#: View/Pages/about.ctp:51;57;58
#: View/Tools/updates.ctp:34
msgid "here"
msgstr ""

#: View/Pages/about.ctp:53
msgid "License"
msgstr ""

#: View/Pages/about.ctp:54
msgid "%s is licensed under the %s."
msgstr ""

#: View/Pages/about.ctp:55
msgid "%s license"
msgstr ""

#: View/Pages/about.ctp:56
msgid "You can find the full text of the license %s and some translations %s."
msgstr ""

#: View/Pages/about.ctp:60
msgid "Translations"
msgstr ""

#: View/Pages/about.ctp:61
msgid "Thanks to %s for the italian translation."
msgstr ""

#: View/Pages/about.ctp:65
msgid "Yes, %s contains an %s. %s was the first to find it."
msgstr ""

#: View/Pages/about.ctp:67
msgid "Libraries, icons, etc"
msgstr ""

#: View/Pages/about.ctp:68
msgid "To develop %s, these libraries have been used:"
msgstr ""

#: View/Pages/about.ctp:70
msgid "%s, a PHP framework for web applications;"
msgstr ""

#: View/Pages/about.ctp:71
msgid "%s, a CakePHP plugin to improve development;"
msgstr ""

#: View/Pages/about.ctp:72
msgid "%s, a JavaScript library;"
msgstr ""

#: View/Pages/about.ctp:73
msgid "%s, a front-end framework for faster and easier web development;"
msgstr ""

#: View/Pages/about.ctp:74
msgid "%s, an HTML5 audio/video player;"
msgstr ""

#: View/Pages/about.ctp:75
msgid "%s, a library to extract Id3 tag from audio and video files;"
msgstr ""

#: View/Pages/about.ctp:76
msgid "%s icons."
msgstr ""

#: View/Pages/shortcuts.ctp:29
msgid "Keyboard shortcuts"
msgstr ""

#: View/Pages/shortcuts.ctp:31
msgid "Using %s as media player, you can use keyboard shortcuts to control the player. Here you can find the function of each key."
msgstr ""

#: View/Pages/shortcuts.ctp:33
msgid "Space"
msgstr ""

#: View/Pages/shortcuts.ctp:33
msgid "Play/pause"
msgstr ""

#: View/Pages/shortcuts.ctp:34
msgid "Enter"
msgstr ""

#: View/Pages/shortcuts.ctp:34
msgid "Full screen"
msgstr ""

#: View/Pages/shortcuts.ctp:35
msgid "Backspace"
msgstr ""

#: View/Pages/shortcuts.ctp:35
msgid "Muted/unmuted"
msgstr ""

#: View/Pages/shortcuts.ctp:36
msgid "More volume"
msgstr ""

#: View/Pages/shortcuts.ctp:37
msgid "Less volume"
msgstr ""

#: View/Pages/shortcuts.ctp:38
msgid "Next track"
msgstr ""

#: View/Pages/shortcuts.ctp:39
msgid "Previous track"
msgstr ""

#: View/Pages/shortcuts.ctp:41
msgid "This works only with video files"
msgstr ""

#: View/Pages/shortcuts.ctp:42
msgid "This works only with playlists"
msgstr ""

#: View/Playlists/add.ctp:29
#: View/Playlists/play_audio.ctp:29
msgid "Audio playlist"
msgstr ""

#: View/Playlists/add.ctp:29
#: View/Playlists/play_video.ctp:29
msgid "Video playlist"
msgstr ""

#: View/Playlists/add.ctp:30
msgid "Create a playlist"
msgstr ""

#: View/Playlists/add.ctp:33
msgid "Playlist name"
msgstr ""

#: View/Playlists/add.ctp:34
#: View/Playlists/index.ctp:33
msgid "Create playlist"
msgstr ""

#: View/Playlists/index.ctp:29
msgid "Audio playlists"
msgstr ""

#: View/Playlists/index.ctp:29
msgid "Video playlists"
msgstr ""

#: View/Playlists/play_as_dir.ctp:29
msgid "Play audio folder"
msgstr ""

#: View/Playlists/play_as_dir.ctp:29
msgid "Play video folder"
msgstr ""

#: View/Playlists/play_audio.ctp:31
#: View/Playlists/play_video.ctp:31
msgid "Delete playlist"
msgstr ""

#: View/Tools/add_source.ctp:28;40
msgid "Add source"
msgstr ""

#: View/Tools/add_source.ctp:28
msgid "Add a new source"
msgstr ""

#: View/Tools/add_source.ctp:32
msgid "Source path"
msgstr ""

#: View/Tools/add_source.ctp:34
msgid "Select a type of source"
msgstr ""

#: View/Tools/add_source.ctp:35
msgid "Source type"
msgstr ""

#: View/Tools/cache.ctp:29
msgid "Manage cache and thumbs"
msgstr ""

#: View/Tools/cache.ctp:33
msgid "The cache is enabled."
msgstr ""

#: View/Tools/cache.ctp:33
msgid "Cache size: %s."
msgstr ""

#: View/Tools/cache.ctp:34
msgid "Note: you should not need to clear the cache, unless you have not changed the configuration or after an upgrade of %s."
msgstr ""

#: View/Tools/cache.ctp:35
msgid "Clear the cache"
msgstr ""

#: View/Tools/cache.ctp:38
msgid "The cache is disabled."
msgstr ""

#: View/Tools/cache.ctp:41
msgid "Thumbs size: %s."
msgstr ""

#: View/Tools/cache.ctp:42
msgid "Note: you should not need to clear thumbnails and that this will slow down the images loading the first time that are displayed. You should clear thumbnails only when they have reached a large size or when many images are no longer used."
msgstr ""

#: View/Tools/cache.ctp:43
msgid "Clear thumbs"
msgstr ""

#: View/Tools/checkup.ctp:29
msgid "Checkup"
msgstr ""

#: View/Tools/checkup.ctp:29
msgid "System checkup"
msgstr ""

#: View/Tools/checkup.ctp:30
msgid "SSMP version: %s"
msgstr ""

#: View/Tools/checkup.ctp:31
msgid "CakePHP version: %s"
msgstr ""

#: View/Tools/checkup.ctp:32
msgid "MeTools version: %s"
msgstr ""

#: View/Tools/checkup.ctp:36;41
msgid "The %s module is enabled"
msgstr ""

#: View/Tools/checkup.ctp:38;43
msgid "The %s module is not enabled"
msgstr ""

#: View/Tools/checkup.ctp:47
msgid "The PHP version is at least %s"
msgstr ""

#: View/Tools/checkup.ctp:49
msgid "The PHP version is less than %s"
msgstr ""

#: View/Tools/checkup.ctp:52;57;62
msgid "The %s extension is enabled"
msgstr ""

#: View/Tools/checkup.ctp:54;59;64
msgid "The %s extension is not enabled"
msgstr ""

#: View/Tools/checkup.ctp:66
msgid "Temporary directory"
msgstr ""

#: View/Tools/checkup.ctp:68
msgid "The temporary directory is readable and writable"
msgstr ""

#: View/Tools/checkup.ctp:70
msgid "The temporary directory is not readable or writable"
msgstr ""

#: View/Tools/checkup.ctp:73
msgid "The cache is enabled"
msgstr ""

#: View/Tools/checkup.ctp:75
msgid "The cache is disabled"
msgstr ""

#: View/Tools/checkup.ctp:78
msgid "The cache is readable and writable"
msgstr ""

#: View/Tools/checkup.ctp:80
msgid "The cache is not readable or writable"
msgstr ""

#: View/Tools/checkup.ctp:83
msgid "The playlist indexes are readable and writable"
msgstr ""

#: View/Tools/checkup.ctp:85
msgid "The playlist indexes are not readable or writable"
msgstr ""

#: View/Tools/checkup.ctp:88
msgid "The thumbs directory is readable and writable"
msgstr ""

#: View/Tools/checkup.ctp:90
msgid "The thumbs directory is not readable or writable"
msgstr ""

#: View/Tools/configuration.ctp:28
msgid "SSMP configuration"
msgstr ""

#: View/Tools/configuration.ctp:32
msgid "App options"
msgstr ""

#: View/Tools/configuration.ctp:35
msgid "Auto"
msgstr ""

#: View/Tools/configuration.ctp:35
msgid "English"
msgstr ""

#: View/Tools/configuration.ctp:35
msgid "Italian"
msgstr ""

#: View/Tools/configuration.ctp:36
msgid "With \"auto\" value, SSMP will use the browser language, if this is available, otherwise it will use english"
msgstr ""

#: View/Tools/configuration.ctp:41
msgid "Show logo"
msgstr ""

#: View/Tools/configuration.ctp:42
msgid "Note that on mobile devices, such as mobile phones or small tablets, however the logo will not be shown"
msgstr ""

#: View/Tools/configuration.ctp:48
msgid "Audio options"
msgstr ""

#: View/Tools/configuration.ctp:51
msgid "Show album art"
msgstr ""

#: View/Tools/configuration.ctp:52
msgid "Show album art for audio files"
msgstr ""

#: View/Tools/configuration.ctp:58;76
msgid "Show ID3 tags"
msgstr ""

#: View/Tools/configuration.ctp:59
msgid "Show ID3 tags for audio files"
msgstr ""

#: View/Tools/configuration.ctp:64
msgid "Image options"
msgstr ""

#: View/Tools/configuration.ctp:67
msgid "Show EXIF data"
msgstr ""

#: View/Tools/configuration.ctp:68
msgid "Show EXIF data for image files"
msgstr ""

#: View/Tools/configuration.ctp:73
msgid "Video options"
msgstr ""

#: View/Tools/configuration.ctp:77
msgid "Show ID3 tags for video files"
msgstr ""

#: View/Tools/configuration.ctp:82
msgid "Video width"
msgstr ""

#: View/Tools/configuration.ctp:83
msgid "With \"auto\" value, will be used the original width of the video. Otherwise, you can specify an integer (greater than 240) that indicates the video width in pixels. Note that in each case, the video may not have a width greater than that of the window"
msgstr ""

#: View/Tools/configuration.ctp:88
msgid "Player options"
msgstr ""

#: View/Tools/configuration.ctp:91
msgid "Enable autoplay"
msgstr ""

#: View/Tools/configuration.ctp:92
msgid "Autoplay media files. Note that this setting will be also applied to playlists"
msgstr ""

#: View/Tools/configuration.ctp:98
msgid "Uses the native player on mobile devices"
msgstr ""

#: View/Tools/configuration.ctp:99
msgid "Will be forced to use the native player on all mobile devices. It is recommended, because the player interacts better with the native controls of mobile devices. If this option is disabled will be used instead of the player shown to the \"type\" in each case"
msgstr ""

#: View/Tools/configuration.ctp:107
msgid "Preload files"
msgstr ""

#: View/Tools/configuration.ctp:108
msgid "SSMP can preload the media file when the page loads. Note that some mobile devices prevent preloading, so that the data traffic is not consumed unnecessarily"
msgstr ""

#: View/Tools/configuration.ctp:115
msgid "Enable shortcuts"
msgstr ""

#: View/Tools/configuration.ctp:116
msgid "Using jPlayer as media player, you can use keyboard shortcuts to control the player. Look at the keyboard %s"
msgstr ""

#: View/Tools/configuration.ctp:117
msgid "shortcuts"
msgstr ""

#: View/Tools/configuration.ctp:122
msgid "Player type"
msgstr ""

#: View/Tools/configuration.ctp:123
msgid "Native"
msgstr ""

#: View/Tools/configuration.ctp:124
msgid "jPlayer offers the same graphic rendering and the same functions on every browser and it also can verify that your browser is able to play a media format. jPlayer is strongly recommended. Also, <strong>if you wanna use playlists</strong> you have to use jPlayer"
msgstr ""

#: View/Tools/configuration.ctp:129
msgid "Default volume"
msgstr ""

#: View/Tools/configuration.ctp:132
msgid "Using jPlayer as media player, you can set the default volume. This must be an integer between \"0\" and \"100\". This will not work on some mobile devices, where the volume control is left to the device"
msgstr ""

#: View/Tools/configuration.ctp:137
msgid "Security options"
msgstr ""

#: View/Tools/configuration.ctp:139
msgid "IP addresses allowed"
msgstr ""

#: View/Tools/configuration.ctp:140;146
msgid "Addresses must be separated by a comma (optionally by a comma and a space). You can use the asterisk (*) as a wildcard. With an empty value, access is granted to any ip addresses (no limitation). See %s before using this option"
msgstr ""

#: View/Tools/configuration.ctp:142;148
#: View/Tools/no_sources.ctp:35
msgid "our wiki"
msgstr ""

#: View/Tools/configuration.ctp:145
msgid "IP addresses denied"
msgstr ""

#: View/Tools/configuration.ctp:151
msgid "Search interval"
msgstr ""

#: View/Tools/configuration.ctp:152
msgid "Time interval required to perform a new search, in seconds. With \"0\" or an empty value, no time interval will be required (no limit)"
msgstr ""

#: View/Tools/configuration.ctp:157
msgid "Save options"
msgstr ""

#: View/Tools/no_sources.ctp:30
msgid "What's wrong?"
msgstr ""

#: View/Tools/no_sources.ctp:32
msgid "It seems that there are no configured sources, or that all sources are invalid!"
msgstr ""

#: View/Tools/no_sources.ctp:33
msgid "First, make sure the file %s exists and is readable."
msgstr ""

#: View/Tools/no_sources.ctp:34
msgid "Then edit this file, adding your sources. If you have any problems, please refer to %s."
msgstr ""

#: View/Tools/no_sources.ctp:36
msgid "Otherwise you can use the interface for the %s and %s."
msgstr ""

#: View/Tools/no_sources.ctp:37
msgid "management of the sources"
msgstr ""

#: View/Tools/no_sources.ctp:38
msgid "add a new source"
msgstr ""

#: View/Tools/no_sources.ctp:41
msgid "Try again"
msgstr ""

#: View/Tools/sources.ctp:30
msgid "You can add %s."
msgstr ""

#: View/Tools/sources.ctp:30
msgid "new sources"
msgstr ""

#: View/Tools/sources.ctp:35
msgid "Audio sources"
msgstr ""

#: View/Tools/sources.ctp:35
msgid "Image sources"
msgstr ""

#: View/Tools/sources.ctp:35
msgid "Video sources"
msgstr ""

#: View/Tools/sources.ctp:38
msgid "Are you sure you want to delete this source?"
msgstr ""

#: View/Tools/sources.ctp:42
msgid "%s is a valid source"
msgstr ""

#: View/Tools/sources.ctp:45
msgid "%s doesn't exist"
msgstr ""

#: View/Tools/sources.ctp:48
msgid "%s exists, but is not readable"
msgstr ""

#: View/Tools/sources.ctp:51
msgid "%s is not a directory"
msgstr ""

#: View/Tools/sources.ctp:54
msgid "%s is a valid source, but it's empty"
msgstr ""

#: View/Tools/updates.ctp:29
msgid "Updates"
msgstr ""

#: View/Tools/updates.ctp:29
msgid "%s updated"
msgstr ""

#: View/Tools/updates.ctp:34
msgid "It's available %s %s. You can get more information and download the new version from %s."
msgstr ""

#: View/Tools/updates.ctp:37
msgid "There are no updates, %s is updated to the latest version."
msgstr ""

#: View/Tools/updates.ctp:40
msgid "Impossible to check for updates. Please, look on %s"
msgstr ""


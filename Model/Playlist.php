<?php

/**
 * Playlist model.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Model
 */
App::uses('AppModel', 'Model');

/**
 * This is not a real model, these rules are only used to check the correctness of forms
 */
class Playlist extends AppModel {
    /**
     * Validation rules
     * @var array Rules
     */
    public $validate = array(
       'name' => array(
          'between' => array(
             'message'  => 'Must be between %d and %d chars', # Error message
             'rule'     => array('between', 3, 100)    # Between 3 and 100 chars
          ),
          'notEmpty' => array(
             'message'  => 'You cannot leave this empty', # Error message
             'rule'     => 'notEmpty'      # Can not be empty
          ),
          'regex' => array(
             'message'  => 'Allowed chars: words, digits, dash, underscore and space', # Error message
             'rule'     => array('custom', '/^[a-z0-9èéòàùì\-_\ ]*$/i')     # Regex
          )
       )
    );

}
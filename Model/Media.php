<?php

/**
 * Media model.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Model
 */
App::uses('AppModel', 'Model');

/**
 * This is not a real model, these rules are only used to check the correctness of forms
 */
class Media extends AppModel {
	/**
	 * Validation rules
	 * @var array Validation rules
	 */
	public $validate = array(
		'p' => array(
			'minLength' => array(
				'allowEmpty'	=> FALSE,						# Can not be empty
				'message'		=> 'Must be at least %d chars',	# Error message
				'rule'			=> array('minLength', 3)		# At least 3 chars
			),
			'noForwardSlash' => array(
				'message'	=> 'The forward slash is a reserved character and can not be used',	# Message
				'rule'		=> array('noForwardSlash')											# Forward slash is reserved
			)
		),
		't' => array(
			'notEmpty' => array(
				'message'	=> 'You cannot leave this empty',	# Error message
				'rule'		=> 'notEmpty'						# Can not be empty
			),
			'inList' => array(
				'message'	=> 'You have to select a valid option',					# Error message
				'rule'		=> array('inList', array('audio', 'image', 'video')),	# Must be a valid value
			)
		)
	);
	
	/**
	 * Checks if the string doesn't contain a forward slash
	 * @param string $check
	 * @return boolean TRUE if the string doesn't contain a forward slash, otherwise FALSE
	 */
	public function noForwardSlash($check) {
		return !strstr(array_values($check)[0], '/');
	}
}
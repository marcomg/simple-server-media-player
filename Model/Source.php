<?php

/**
 * Source model.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Model
 */
App::uses('AppModel', 'Model');

/**
 * This is not a real model, these rules are only used to check the correctness of forms
 */
class Source extends AppModel {
	/**
	 * Validation rules
	 * @var array Validation rules
	 */
	public $validate = array(
		//Source path
		'path' => array(
			'notEmpty' => array(
				'message'	=> 'You cannot leave this empty',	# Error message
				'rule'		=> 'notEmpty'						# Can not be empty
			),
			'sourceExists' => array(
				'message'	=> 'The directory doesn\'t exist',	# Error message
				'rule'		=> array('sourceExists')			# The directory should exist
			),
			'sourceIsReadable' => array(
				'message'	=> 'The directory exists, but is not readable',	# Error message
				'rule'		=> array('sourceIsReadable')					# The directory should exist
			),
			'sourceIsDir' => array(
				'message'	=> 'The path doesn\'t match a directory',	# Error message
				'rule'		=> array('sourceIsDir')						# The directory should exist
			),
		),
		//Source type
		'type' => array(
			'notEmpty' => array(
				'message'	=> 'You cannot leave this empty',	# Error message
				'rule'		=> 'notEmpty'						# Can not be empty
			),
			'inList' => array(
				'message'	=> 'You have to select a valid option',					# Error message
				'rule'		=> array('inList', array('audio', 'image', 'video')),	# Must be a valid value
			)
		)
	);
	
	/**
	 * Checks if the source exists
	 * @param string $check
	 * @return boolean TRUE if the source exists, otherwise FALSE
	 */
	public function sourceExists($check) {
		return file_exists(array_values($check)[0]);
	}
	
	/**
	 * Checks if the source is readable
	 * @param string $check
	 * @return boolean TRUE if the source is readable, otherwise FALSE
	 */
	public function sourceIsReadable($check) {
		return is_readable(array_values($check)[0]);
	}
	
	/**
	 * Checks if the source is a directory
	 * @param string $check
	 * @return boolean TRUE if the source is a directory, otherwise FALSE
	 */
	public function sourceIsDir($check) {
		return is_dir(array_values($check)[0]);
	}
}
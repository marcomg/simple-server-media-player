<?php

/**
 * Configuration model.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Model
 */
App::uses('AppModel', 'Model');

/**
 * This is not a real model, these rules are only used to check the correctness of forms
 */
class Configuration extends AppModel {
	/**
	 * Validation rules
	 * @var array Validation rules
	 */
	public $validate = array(
		'app_language' => array(
			'notEmpty' => array(
				'message'	=> 'You cannot leave this empty',	# Error message
				'rule'		=> 'notEmpty'						# Can not be empty
			),
			'inList' => array(
				'message'	=> 'You have to select a valid option',				# Error message
				'rule'		=> array('inList', array('auto', 'ita', 'eng')),	# Must be a valid value
			)
		),
		'app_logo' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'audio_albumArt' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'audio_id3Tags' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'image_exifData' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'video_id3Tags' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'video_width' => array(
			'message'	=> 'It must be equal to "auto" or an integer greater than 240',	# Error message
			'rule'		=> 'checkVideoWidth',											# Must be "auto" or >= 240
		),
		'player_autoplay' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'player_nativeOnMobile' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'player_preload' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'player_shortcuts' => array(
			'message'	=> 'You have to select a valid option',		# Error message
			'rule'		=> array('inList', array('true', 'false')),	# Must be a valid value
		),
		'player_type' => array(
			'notEmpty' => array(
				'message'	=> 'You cannot leave this empty',	# Error message
				'rule'		=> 'notEmpty'						# Can not be empty
			),
			'inList' => array(
				'message'	=> 'You have to select a valid option',			# Error message
				'rule'		=> array('inList', array('jplayer', 'native')),	# Must be a valid value
			)
		),
		'player_volume' => array(
			'integer' => array(
				'message' => 'It must be an integer',	# Error message
				'rule'    => '/^[0-9]+$/'				# Must be an integer
			),
			'range' => array(
				'message'	=> 'It must be a number between 0 and 100',	# Error message
				'rule'		=> array('range', -1, 101),					# Must be a number between 0 and 100
			)
		),
		'security_ipAllowed' => array(
			'allowEmpty'	=> TRUE,																														# Can be empty
			'message'		=> 'The syntax is not valid',	# Error message
			'rule'			=> 'checkIpSyntax',				# Must have a valid syntax
		),
		'security_ipDenied' => array(
			'allowEmpty'	=> TRUE,																														# Can be empty
			'message'		=> 'The syntax is not valid',	# Error message
			'rule'			=> 'checkIpSyntax',				# Must have a valid syntax																												# Must have a valid syntax
		),
		'security_searchInterval' => array(
			'allowEmpty'	=> TRUE,					# Can be empty
			'message'		=> 'It must be an integer',	# Error message
			'rule'			=> '/^[0-9]+$/'				# Must be an integer
		)
	);
	
	/**
	 * Checks if the video width is equal to "auto" or if is an integer greater than 240
	 * @param string $check
	 * @return boolean TRUE if is equal to "auto" or if is an integer greater than 240, otherwise FALSE
	 */
	public function checkVideoWidth($check) {
		if(($check = array_values($check)[0]) == 'auto')
			return TRUE;
		
		if(preg_match('/^[0-9]+$/', $check) && $check >= 240)
			return TRUE;
		
		return FALSE;
	}
	
	/**
	 * Checks if the ip syntax is valid
	 * @param string $check
	 * @return boolean TRUE if the syntax is valid, otherwise FALSE
	 */
	public function checkIpSyntax($check) {
		return preg_match('/^(([1-9][0-9]{0,2}|\*)\.([0-9]{1,3}|\*)\.([0-9]{1,3}|\*)\.([0-9]{1,3}|\*),?\s?)+$/', array_values($check)[0]);
	}
}
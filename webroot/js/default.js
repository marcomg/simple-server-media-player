/**
 * This file is part of MeTools.
 *
 * MeTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MeTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it Nova Atlantis Ltd
 */

/**
 * Add a media file to a playlist
 * @param Obj element
 * @param Obj event
 */
function addToPlaylist(element, event) {
	event.preventDefault();

	//Gets the ID of the selected playlist
	var playlistId = $('#playlist-id').val();
	
	//Gets the action url
	var url = $(element).data('url');

	if(url) {
		var icon = $('i', element);
		icon.attr('class', 'fa fa-spinner fa-spin');
		
		$.ajax({ dataType : 'json', url: url+'/id:'+playlistId }).done(function(response) {
			if(response) {
				icon.attr('class', 'fa fa-check');
				$(element).removeAttr('data-url').removeAttr('onclick').delay(2000).fadeOut();
			}
			else
				icon.attr('class', 'fa fa-times').css('color', 'red');
		});
	}

	return true;
}

/**
 * Shows the form to add media files to a playlist
 * @param Obj event
 */
function showAddToPlaylist(event) {
	event.preventDefault();

	//Hides all buttons to media files to a playlist
	$('.browser-view .add-to-playlist-btn').hide();

	//If is already visible
	if($('#add-to-playlist').is(':visible')) {
		//Slides up (hide) and, when the animation is complete, resets the playlist id
		$('#add-to-playlist').slideUp('slow', function() { $('#playlist-id').val(null); });
	}
	else {
		//Reset the playlist id and slides down (show)
		$('#playlist-id').val(null);
		$('#add-to-playlist').slideDown('slow');
	}

	return true;
}

/**
 * Shows buttons to add a media file to a playlist
 */
function showAddToPlaylistBtn() {
	//If the playlist value is valid, shows buttons
	if($('#playlist-id').val())
		$('.browser-view .add-to-playlist-btn').fadeIn('fast');
	//Else, hides buttons
	else
		$('.browser-view .add-to-playlist-btn').fadeOut('fast');

	return true;
}

$(function() {
	//With audio and the native player, on click on the album art it plays audio if it is paused or vice versa
	$('.native-player.native-audio .album-art').click(function(event) {
		event.preventDefault();
		var audio = $(this).next('audio')[0];
		if(audio.paused)
			audio.play();
		else
			audio.pause();
	});
	
	//On click on the button to show the search form
	$('#show-search-form').click(function(event) {
		event.preventDefault();
		$('#search-form').slideToggle('slow');
	});
	
	//On click on a list group of the browser view, adds the "active" class
	$('.browser-view.list-group .list-group-item').click(function() {
		$('.browser-view.list-group .list-group-item.active').removeClass('active');
		$(this).addClass('active');
	});
	
	//On hover on a source element in the sources list, toggle the remove link
	$('#sources .source').hover(function() {
		$('a', this).toggle();
	});
});
# 2.x branch
## 2.0 branch
### 2.0.2
* you can now manage, add and remove sources via GUI;
* you can now set the SSMP configuration via GUI;
* added the "no sources" page, which informs the user and indicates solutions;
* now you can force the use of the native player with mobile devices. This offers greater compatibility;
* restyled the system check up page;
* improved routes, urls are now shorter and more readable;
* reorganized the parameters of configurations, now easier to use;
* the packaged version of SSMP requires and works with the CakePHP package;
* fixed bug, SSMP now correctly uses the browser language by default ([#13](http://git.novatlantis.it/simple-server-media-player/issue/13/auto-language-doesnt-work));
* fixed bug with empty playlists ([#12](http://git.novatlantis.it/simple-server-media-player/issue/12/no-error-on-empty-playlist));
* fixed bug when playing playlists ([#9](http://git.novatlantis.it/simple-server-media-player/issue/9/bug-when-playing-playlists));
* updated Bootstrap to 3.1.1 version.

### 2.0.1
* ID3 tags for video files (*mp4* and *webm*, not instead for *ogv*);
* completely rewritten the jPlayer integration, now more powerful and flexible. The appearance of the player has been restyled;
* completely rewritten the request parser, now more flexible and faster;
* now you can choose separately which player to use for audio files and which for video files;
* using the native player with audio files, now you can play/pause by clicking on the album art;
* now the browser will ignore directories and files that are not readable;
* as well as specifying what ip addresses are enabled, you can now specify denied addresses;
* you can choose, using the configuration file, if you want to preload audio and video files;
* XML files are used for the sources list and for the configuration, they are simple and easy to handle;
* the sources list and the configuration are cached;
* thumbnails are directly handled by the `ThumbsController` provided by MeTools;
* CSS and JS files are combined, thanks to a special script;
* improved support for ID3 tags, now shows more values;
* now the album art for audio files is also shown with JPlayer playlist ([fixed #5](http://git.novatlantis.it/simple-server-media-player/issue/5));
* removed the search through "locate" for Unix system (the native search is always faster);
* improved the build script, which now removes many useless files;
* updated CakePHP to 2.4.5, jQuery to 2.1.0, Bootstrap to 3.1.0, Font Awesome to 4.0.3, jPlayer to 2.5.0 and getID3 to 1.9.7;
* the compatibility table has been removed (it's available on SSMP website).

# 1.x branch
## 1.5 branch
### 1.5.0
* support for playlists: you can create, delete, view and play playlists;
* massive code refactoring. Components are used instead of models;
* simplified and improved the getting multimedia sources;
* sped up the ip control;
* updated Bootstrap to 2.3.2 version;
* updated Font Awesome icons to 3.1.1 version;
* updated Cakephp to 2.3.5 version.

## 1.4 branch
### 1.4.1
* now you can allow access based on IP addresses;
* now you can set a time interval required to perform a new search;
* added shortcuts and the page with the shortcuts explanation;
* now with JPlayer can play or pause the video by clicking on it;
* customized (and lightened) Bootstrap for SSMP;
* fixed a bug in displaying images on mobile devices;
* fixed a bug with Midori in full screen mode;
* fixed the video height on mobile devices, now is correctly set.

### 1.4.0
* added jPlayer, an alternative to the native player. JPlayer will be the default player;
* playlists work now with jPlayer;
* for search, now you can use php native functions or (only on Unix systems) locate engine;
* now hidden files and directories are ignored, both when browsing sources, both when searching files;
* updated Cakephp to 2.3.4 version and jQuery to 2.0 version;
* archives have been optimized, deleting uncompressed files.

## 1.3 branch
### 1.3.1
* internal search engine, with support for regex;
* album art for audio files and playlists.

### 1.3.0
* thanks to the Config/ssmp.php file, you can choose the video width;
* updated Cakephp to 2.3.2 version;
* now are used only Font awesome icons;
* updated browsers compatibility table;
* fixed a bug related to the audio player width on mobile phones;
* fixed a bug with autoplay disabled;
* fixed photos max width for large tablets.

## 1.2 branch
### 1.2.2
* added italian translation;
* added the system check up;
* you can now empty the thumbnail cache;
* fixed bug when empty cache and thumbs;
* many improvements to the code and fixed minor bugs.

### 1.2.1
* now shows id3 datas for audio files;
* thanks to the Config/ssmp.php file, you can choose: how many thumbnails display per row, whether to show exif data, whether to show id3 tags, whether to autoplay audio/video files, whether to show SSMP logo;
* added compatibility table for audio/video formats (look on Tools -> Supported formats);
* now you can manage and empty thumbnails;
* added the compatibility table (Tools -> Supported formats);
* fixed a bug with videos width on mobile devices;
* fixed a bug in the size of photos with Chrome/Chromium browser ([report](http://www.it-opensuse.org/post15003.html#p15003));
* fixed bug: show the button to play the directory only if there're files in the current directory.

### 1.2.0
* can now play a whole directory (with audio or video files) as a playlist;
* true miniatures are generated for each request, in the thumbnails view and in the view of a single photo;
* thanks to the *expires* module, now SSMP can better manage the browser cache.

## 1.1 branch
### 1.1.0
* support for exif data;
* the layout is much more fluid;
* the layout has been extensively optimized for mobile devices and tablets;
* is used a fixed topbar;
* photos can now be displayed as thumbnails. A specific button lets you switch between display modes;
* updated and optimized Tango icons;
* all images are now correctly displayed;
* support for jpe image files (image/jpeg);
* SSMP checks if there is at least one source configured before starting;
* fixed some minor bugs, optimized and clean the source, fix some graphical mistakes.

## 1.0 branch
### 1.0.2
* fixed bug in resizing images with some browser ([screenshot](http://www.it-opensuse.org/post14906.html#p14906));
* fixed typos.

### 1.0.1-beta
First release.
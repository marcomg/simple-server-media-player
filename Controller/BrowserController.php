<?php

/**
 * BrowserController.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Controller
 */
App::uses('AppController', 'Controller');

/**
 * It browses sources, directories, plays files and searches through the file system
 */
class BrowserController extends AppController {
    /**
     * Components
     * @var array
     */
    public $components = array('Explorer');

    /**
     * Checks that the latest search has been executed out of the minimum interval.
     * @uses config to get the configuration
     * @return boolean TRUE if the latest search has been executed out of the minimum interval, otherwise FALSE
     */
    private function __checkLastSearch() {
        $interval = $this->config['security']['searchInterval'];

        if(empty($interval))
            return TRUE;

        //Gets the last search timestamp from the session 
        $last = $this->Session->read('lastSearch');

        //If there was a previous search and if this was done before the minimum interval
        if($last && ($last + $interval) > time())
            return FALSE;

        //In any other case, saves the timestamp of the current search and returns TRUE
        $this->Session->write('lastSearch', time());
        return TRUE;
    }	
	
	/**
     * Called before the controller action.
	 * @uses action to get the action name
     * @uses data to set the request data
     * @uses __getRequest() to get the request data
	 */
    public function beforeFilter() {
		parent::beforeFilter();

		//Sets the request data
		if($this->action != "search")
			$this->data = $this->__getRequest();
	}

	/**
     * Views sources list.
     * @uses data to get the request data
     * @uses sources to get list of valid sources
     */
    public function sources() {
        $this->set('sources', $this->sources[$this->data['type']]);
    }

    /**
     * Views a directory content.
	 * @uses action to get the action name
     * @uses data to get the request data
	 * @uses ExplorerComponent::getDirs() to get the directories list
	 * @uses ExplorerComponent::getFiles() to get the files list
     */
    public function browse() {
        $this->set(array(
           'dirs'   => $this->Explorer->getDirs($this->data['full_path']),
           'files'  => $this->Explorer->getFiles($this->data['full_path'], $this->data['type'])
        ));

        $this->render($this->action.'_'.$this->data['type']);
    }

    /**
     * Downloads a file.
     * @return object file response
     * @uses data to get the request data
     */
    public function download() {
        $this->response->file($this->data['full_path'], array('download' => TRUE));
        return $this->response;
    }

    /**
     * Plays (plays audios/video and display images) a file.
	 * @uses action to get the action name
     * @uses config to get the configuration
     * @uses data to get the request data
	 * @uses ExplorerComponent::getAlbumArt() to get the album art
	 * @uses ExplorerComponent::getExifData() to get the Exif data
	 * @uses ExplorerComponent::getFileInfo() to get the info about the file
	 * @uses ExplorerComponent::getId3Data() to get the Id3 data
	 * 
     */
    public function play() {
        $this->set('info', $this->Explorer->getFileInfo($this->data['full_path']));

        //If is an audio file 
        if($this->data['type'] == 'audio') {
            //If Id3 tags are enabled
            if($this->config['audio']['id3Tags'])
                $this->set('id3', $this->Explorer->getId3Data($this->data['full_path']));

            $this->set('albumArt', $this->Explorer->getAlbumArt($this->data['full_path']));
        }
        //Else, if is an image and exif data is enabled
        elseif($this->data['type'] == 'image' && $this->config['image']['exifData'])
            $this->set('exif', $this->Explorer->getExifData($this->data['full_path']));
        //Else, if is a video file and Id3 tags are enabled
        elseif($this->data['type'] == 'video' && $this->config['video']['id3Tags'])
            $this->set('id3', $this->Explorer->getId3Data($this->data['full_path']));

        $this->render($this->action.'_'.$this->data['type']);
    }

    /**
     * Searches files.
	 * @uses config to get the configuration
	 * @uses __checkLastSearch() to check if the latest search has been executed out of the minimum interval
	 * @uses ExplorerComponent::search() to perform the search
     */
    public function search() {
		if($this->request->is('post')) {
			//If the latest search was done before the interval minimum of time
			if($this->__checkLastSearch()) {
				//Loads and sets the Media model
				$this->loadModel('Media');
				$this->Media->set($this->request->data);
				if($this->Media->validates()) {
					$data = $this->request->data['Media'];

					//Sets start time
					$start = microtime(TRUE);
					$results = $this->Explorer->search($data['p'], $data['t'], empty($data['r']) ? FALSE : TRUE);

					$this->set(array(
						'exec_time' => microtime(TRUE) - $start,
						'results'	=> $results,
						'type'		=> $data['t']
					));
				}
			}
			//Else, if the last search has not been done before the interval minimum of time, sets an alert
			else
				$this->Session->flash(__('You can perform a search every %s seconds', $this->config['security']['searchInterval']), 'alert');
		}
	}
}
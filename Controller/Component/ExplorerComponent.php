<?php

/**
 * ExplorerComponent.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Controller\Component
 */
App::uses('Component', 'Controller');

/**
 * The Explorer component handles the file system, getting files, directories and their details.
 */
class ExplorerComponent extends Component {
    /**
     * Supported extensions
     * @var array
     */
    protected $extension = array(
       'audio' => array('mp3', 'ogg', 'wav'),
       'image' => array('jpg', 'jpeg', 'jpe', 'png', 'gif'),
       'video' => array('mp4', 'ogv', 'webm')
    );

    /**
     * Called before the Controller::beforeFilter().
     * @param Controller $controller Controller with components to initialize
     */
    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    /**
     * Gets the album art.
     * @param string $path Album/file path
     * @return mixed Album art, if it was found. Otherwise FALSE
     */
    public function getAlbumArt($path) {
        //If the path is not a directory, sets the directory
        $path = is_dir($path) ? $path : dirname($path);

        $folder = new Folder($path);

        //Searches for a picture with an appropriate name and return the first
        if($file = $folder->find('(cover|folder|album(-|_)?art((-|_)?small)?|front)\.(jpg|jpeg|gif|png)'))
            $file = $file[0];
        //Else, search for any picture and return the first
        elseif($file = $folder->find('.+\.(jpg|jpeg|gif|png)'))
            $file = $file[0];

        if(!empty($file))
            return $path.DS.$file;

        return FALSE;
    }

    /**
     * Gets the directories list for a path.
     * @param string $path Directory path
     * @return array Directories list
     */
    public function getDirs($path) {
        $folder = new Folder($path);
        $dirs = $folder->read(TRUE, TRUE);

        //Removes directories that are not readable
        array_walk($dirs[0], function(&$v, $k, $path) {
            $v = is_readable(Folder::slashTerm($path).$v) ? $v : FALSE;
        }, $path);
        $dirs = array_filter($dirs[0]);

        return $dirs;
    }

    /**
     * Gets the Exif data.
     * @param string $path File path
     * @return mixed Exif data as array or FALSE
     */
    public function getExifData($path) {
        if(!function_exists('exif_read_data'))
            return FALSE;

        //Gets Exif data
        $data = @exif_read_data($path, NULL, TRUE);

        if(empty($data))
            return FALSE;

        //Removes useless data
		unset(
			$data['COMPUTED']['ByteOrderMotorola'],
			$data['COMPUTED']['html'],
			$data['COMPUTED']['Thumbnail.FileType'],
			$data['COMPUTED']['Thumbnail.MimeType'],
			$data['EXIF']['ComponentsConfiguration'],
			$data['FILE'], 
			$data['INTEROP'],
			$data['THUMBNAIL'],
			$data['WINXP']
		);
		
        array_walk($data, function(&$v) {			
            array_walk($v, function(&$v, $k) {
				//If the key is called "undefinedtag"
				if(preg_match('/undefinedtag.*/i', $k))
					$v = FALSE;
				elseif(is_string($v)) {
					//If the value is not utf-8
					if(!preg_match('!\S!u', $v))
						$v = FALSE;
					//If the value is "UNDEFINED" string
					elseif($v === 'UNDEFINED')
						$v = FALSE;
				}
            });
        });

        return array_map('array_filter', $data);
    }

    /**
     * Gets the file info.
     * @param string $path File path
     * @return array File info
     */
    public function getFileInfo($path) {
        $file = new File($path);
        $info = $file->info();

        //Converts some values
        $info['filesize'] = CakeNumber::toReadableSize($info['filesize']);

        return $info;
    }

    /**
     * Gets the files list in a path for a type.
     * @param string $path Directory path
     * @param string $type Type (`audio`, `image` or `video`)
     * @return array Files list
	 * @uses extensions to get the supported extensions
     */
    public function getFiles($path, $type) {
        $folder = new Folder($path);
        $files = $folder->find('[^\.]{1}.*\.('.implode('|', $this->extension[$type]).')', TRUE);
		
        //Removes files that are not readable
        array_walk($files, function(&$v, $k, $path) { $v = is_readable(Folder::slashTerm($path).$v) ? $v : FALSE; }, $path);
        $files = array_filter($files);
		
        return $files;
    }
	
	/**
     * Gets the Id3 data for an audio file.
     * @param string $path File path
     * @return array Id3 data
	 * @throws InternalErrorException
	 */
    public function getId3Data($path) {
        $file = new File($path);

        //No support for ogv files
        if(($ext = $file->ext()) == 'ogv')
            return FALSE;

        //Loads Vendor/getid3/getid3.php and gets Id3 data
        App::import('Vendor', 'getid3/getid3');
        $getID3 = new getID3();
        $tmpData = $getID3->analyze($path);

        //Removes useless data and encodes to UTF-8
        unset($tmpData['comments']['picture'], $tmpData['id3v2']['APIC'], $tmpData['riff'], $tmpData['flac']['PICTURE'], $tmpData['quicktime']['moov']['subatoms'], $tmpData['matroska']['info'], $tmpData['matroska']['tracks']['tracks']);
        array_walk_recursive($tmpData, function(&$v) { $v = utf8_encode($v); });

        if(!empty($tmpData['error'][0]))
            throw new InternalErrorException($tmpData['error'][0]);

        $data = array();

        //If this is an audio file, searches where the tags are located
        if(in_array($ext, array('mp3', 'ogg', 'wav'))) {
            if(!empty($tmpData['id3v2']['comments']))
                $data = $tmpData['id3v2']['comments'];
            elseif(!empty($tmpData['tags']['id3v2']))
                $data = $tmpData['tags']['id3v2'];
            elseif(!empty($tmpData['id3v1']['comments']))
                $data = $tmpData['id3v1']['comments'];
            elseif(!empty($tmpData['tags']['id3v1']))
                $data = $tmpData['tags']['id3v1'];
            elseif(!empty($tmpData['tags']['vorbiscomment']))
                $data = $tmpData['tags']['vorbiscomment'];
            elseif(!empty($tmpData['ogg']['comments']))
                $data = $tmpData['ogg']['comments'];

            $data = empty($data) ? array() : array_map(function($v) { return $v[0]; }, $data);

            //Adds/converts some values
            $data['fileformat'] = empty($tmpData['fileformat']) ? NULL : $tmpData['fileformat'];
            $data['channels'] = empty($tmpData['audio']['channels']) ? NULL : $tmpData['audio']['channels'];
            $data['sample_rate'] = empty($tmpData['audio']['sample_rate']) ? NULL : sprintf('%s Hz', preg_replace('/([0-9]{2})([0-9]{3})/', '$1,$2', $tmpData['audio']['sample_rate']));
            $data['channelmode'] = empty($tmpData['audio']['channelmode']) ? NULL : $tmpData['audio']['channelmode'];
            $data['codec'] = empty($tmpData['audio']['codec']) ? NULL : $tmpData['audio']['codec'];
            $data['encoder'] = empty($tmpData['audio']['encoder']) ? NULL : $tmpData['audio']['encoder'];
            $data['bitrate'] = empty($tmpData['bitrate']) ? NULL : sprintf('%s kb/s', number_format($tmpData['bitrate'] / 1024));
        }
        //Else, if this is a video file, searches where the tags are located
        else {
            if(!empty($tmpData['audio']) && !empty($tmpData['video']))
                $data = array('audio' => $tmpData['audio'], 'video' => $tmpData['video']);
            elseif(!empty($tmpData['quicktime']['audio']) && !empty($tmpData['quicktime']['video']))
                $data = array('audio' => $tmpData['quicktime']['audio'], 'video' => $tmpData['quicktime']['video']);

            //Adds/converts some values
            $data['audio']['sample_rate'] = empty($data['audio']['sample_rate']) ? NULL : sprintf('%s Hz', preg_replace('/([0-9]{2})([0-9]{3})/', '$1,$2', $data['audio']['sample_rate']));
            $data['audio']['bitrate'] = empty($data['audio']['bitrate']) ? NULL : sprintf('%s kb/s', number_format($data['audio']['bitrate'] / 1024));
            $data['video']['bitrate'] = empty($data['video']['bitrate']) ? NULL : sprintf('%s kb/s', number_format($data['video']['bitrate'] / 1024));
            $data['video']['frame_rate'] = empty($data['video']['frame_rate']) ? NULL : sprintf('%s fps', $data['video']['frame_rate']);
        }

        $data['playtime'] = empty($tmpData['playtime_string']) ? NULL : $tmpData['playtime_string'];

        return $data;
    }
	
	/**
	 * Internal search function.
	 * 
	 * It needs to be run without the `$path` argument, which is automatically 
	 * added when the function is called cyclically.
     * @param string $pattern Pattern
	 * @param string $source Source
	 * @param string $path Path
	 * @return array Results
	 * @uses __search() to search recursively
	 */
	private function __search($pattern, $source, $path = NULL) {
		$folder = new Folder(empty($path) ? $source : $source.$path);
		
		$results = array();
		
		list($dirs, $files) = $folder->read(true, true);
		
		foreach($files as $filename) {
			if(preg_match($pattern, $filename))
				$results[] = compact('path', 'filename');
		}
		
		foreach($dirs as $dir)
			$results = am($results, $this->__search($pattern, $source, empty($path) ? $dir : $path.DS.$dir));
		
		return $results;
	}

    /**
     * Searches files for media type.
     * @param string $pattern Pattern
     * @param string $type Media type
     * @param bool $regex Is a regex?
     * @return array Results (files sorted by source)
	 * @uses extensions to get the supported extensions
	 * @uses __search() to search
     */
    public function search($pattern, $type, $regex = FALSE) {
        //If was not requested to use a regex, quotes the pattern
		$pattern = !$regex ? preg_quote($pattern, '/') : $pattern;
		$pattern = '/^.*'.$pattern.'.*\.('.implode('|', $this->extension[$type]).')$/i';
		
        $results = array();
		
		foreach($this->controller->sources[$type] as $source) {
			$files = $this->__search($pattern, $source);
			
			if(!empty($files))
				$results[] = compact('source', 'files');
		}
		
		return $results;
    }
}
<?php

/**
 * PlaylistsController.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Controller
 */
App::uses('AppController', 'Controller');
App::uses('FileArray', 'MeTools.Utility');

/**
 * It handles the playlists. It can play, list, create and delete playlists, add 
 * media files to a playlists and play whole directories as a playlist
 */
class PlaylistsController extends AppController {
    /**
     * Components
     * @var array
     */
    public $components = array('Explorer');
	
	/**
	 * Playlists paths
	 * @var string
	 */
	private $path;
	
	/**
	 * Playlists indexes
	 * @var string
	 */
	private $index;

    /**
     * Checks if jPlayer is the selected media player.
	 * @uses config to get the configuration
     */
    private function __checkForJplayer() {
        //Redirects without jPlayer
        if($this->config['player']['type'] != 'jplayer') {
            $this->Session->flash(__('In order to use playlists, you have to use jPlayer as media player'), 'alert');
            $this->redirect('/');
        }
    }

    /**
     * Called before the controller action.
     * @uses data to set the request data
	 * @uses index to set the playlist indexes 
	 * @uses path to set the playlist paths
	 * @uses __getRequest() to get the request data
     */
    public function beforeFilter() {
        parent::beforeFilter();

        //Sets the request data
        $this->data = $this->__getRequest();

        //Sets playlist paths, which contains playlist indexes and playlist files
        $this->path = array('audio' => TMP.'playlists'.DS.'audio', 'video' => TMP.'playlists'.DS.'video');
        //Sets playlist indexes, which contain playlist names
        $this->index = array('audio' => $this->path['audio'].DS.'index', 'video' => $this->path['video'].DS.'index');
    }

    /**
     * Lists all playlists.
     * @return array Playlists if called by requestAction()
     * @uses data to get the request data
	 * @uses index to get the playlist indexes
	 * @uses __checkForJplayer() to check if jPlayer is the selected media player
     */
    public function index() {
        $playlists = new FileArray($this->index[$this->data['type']]);
        $playlists = $playlists->getAll();

        //If the action is called by requestAction(), returns playlists
        if(!empty($this->request->params['requested']))
            return $playlists;

        $this->__checkForJplayer();

        $this->set(compact('playlists'));
    }

    /**
     * Adds a playlist.
     * @uses data to get the request data
	 * @uses index to get the playlist indexes
	 * @uses __checkForJplayer() to check if jPlayer is the selected media player
     */
    public function add() {
        $this->__checkForJplayer();

        if($this->request->is('post')) {
            //Sets the Playlist model
            $this->Playlist->set($this->request->data);
            if($this->Playlist->validates()) {
                $playlist = new FileArray($this->index[$type = $this->data['type']]);

                if(!$playlist->exists($name = $this->request->data['Playlist']['name'])) {
                    if($playlist->insert($name)) {
                        $this->Session->flash(__('The playlist has been created'), 'success');
                        $this->redirect(array('action' => 'index', $type));
                    }
                    else
                        $this->Session->flash(__('The playlist has not been created'), 'error');
                }
                else
                    $this->Session->flash(__('A playlist with this name already exists'), 'error');
            }
        }
    }

    /**
     * Deletes a playlist.
     * @uses data to get the request data
	 * @uses index to get the playlist indexes
	 * @uses path to get the playlist paths
     */
    public function delete() {
        //Deletes the playlist file
        $playlist = new File($this->path[$type = $this->data['type']].DS.($id = $this->passedArgs['id']));
        $playlist->delete();

        //Deletes the playlist from the index
        $playlists = new FileArray($this->index[$type]);
        if($playlists->delete($id))
            $this->Session->flash(__('The playlist has been deleted'), 'success');
        else
            $this->Session->flash(__('The playlist has not been deleted'), 'error');

        $this->redirect(array('action' => 'index', $type));
    }

	/**
     * Adds a media to a playlist.
     * This is an API and should be called by ajax
	 * @throws ForbiddenException
     * @uses data to get the request data
	 * @uses path to get the playlist paths
	 * @uses __checkForJplayer() to check if jPlayer is the selected media player
	 */
    public function addToPlaylistAPI() {
        if(!$this->request->is('ajax'))
            throw new ForbiddenException(__('This action should be called by Ajax'));

        $this->__checkForJplayer();

        $playlist = new FileArray($this->path[$this->data['type']].DS.$this->request->params['named']['id']);

        $this->set('result', $playlist->insert($this->data) ? TRUE : FALSE);
        $this->set('_serialize', 'result');
    }

    /**
     * Plays a playlist.
	 * @uses action to get the action name
     * @uses data to get the request data
	 * @uses index to get the playlist indexes paths
	 * @uses path to get the playlist directories paths
	 * @uses __checkForJplayer() to check if jPlayer is the selected media player
	 * @uses ExplorerComponent::getAlbumArt to get the album art
     */
    public function play() {		
        $this->__checkForJplayer();
		
        //Gets the playlist name
        $playlists = new FileArray($this->index[$type = $this->data['type']]);
        $name = $playlists->findByKey($id = $this->request->params['id']);

        //Gets the playlist files
        $playlist = new FileArray($this->path[$type].DS.$id);

        $files = array();
        foreach($playlist->find('all') as $file) {
            $albumArt = $this->Explorer->getAlbumArt($file['full_path']);
            $url = Router::url(am(array('controller' => 'browser', 'action' => 'download', $file['type'], $file['id']), explode('/', $file['path']), array($file['filename'])), TRUE);
			$files[] = array('albumArt' => $albumArt, 'filename' => $file['filename'], 'url' => $url);
        }

        $this->set(compact('files', 'id', 'name'));

        $this->render($this->action.'_'.$this->data['type']);
    }

    /**
     * Plays a whole directory as a playlist.
     * @uses data to get the request data
	 * @uses __checkForJplayer() to check if jPlayer is the selected media player
	 * @uses ExplorerComponent::getAlbumArt to get the album art
     */
    public function play_as_dir() {
        $this->__checkForJplayer();

        $files = $this->Explorer->getFiles($path = $this->data['full_path'], $type = $this->data['type']);
        $albumArt = $type == 'audio' ? $this->Explorer->getAlbumArt($path) : FALSE;

        //For each file, it adds the full path and album art
        array_walk($files, function(&$v, $k, $albumArt) {
            $v = array(
               'albumArt'   => $albumArt,
               'filename'   => $v,
               'url'        => Router::url(am(array('controller' => 'browser', 'action' => 'download', $this->data['type'], $this->data['id']), explode(DS, $this->data['path']), $v), TRUE)
            );
        }, $albumArt);

        $this->set(compact('files'));
    }
}
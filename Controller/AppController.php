<?php

/**
 * AppController.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Controller
 */
App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');

/**
 * Application level controller.
 */
class AppController extends Controller {
    /**
     * Components
     * @var array Components
     */
    public $components = array('RequestHandler', 'Session' => array('className' => 'MeTools.MeSession'), 'Xml' => array('className' => 'MeTools.Xml'));

    /**
     * Helpers
     * @var array Helpers
     */
    public $helpers = array('Form' => array('className' => 'MeTools.MeForm'), 'Html' => array('className' => 'MeTools.MeHtml'));

	/**
	 * Current action name
	 * @var string Action name
	 */
	protected $action = FALSE;
	
    /**
     * Configuration
     * @var array Configuration
     */
    protected $config = array();
	
	/**
	 * Current controller name
	 * @var string Controller name
	 */
	protected $controller = FALSE;

    /**
     * Request data
     * @var mixed Request data
     */
    protected $data = array();

    /**
     * Default configuration
     * @var array Default configuration
     */
    private $default = array(
		'app' => array('fortune' => FALSE, 'language' => 'auto', 'logo' => TRUE),
		'audio' => array('albumArt' => TRUE, 'id3Tags' => TRUE),
		'image' => array('exifData' => TRUE),
		'player' => array('autoplay' => TRUE, 'nativeOnMobile' => TRUE, 'preload' => TRUE, 'shortcuts' => TRUE, 'type' => 'jplayer', 'volume' => '100'),
		'security' => array('ipAllowed' => FALSE, 'ipDenied' => FALSE, 'searchInterval' => FALSE),
		'video' => array('id3Tags' => FALSE, 'width' => 'auto')
	);

	/**
     * List of valid sources
     * @var array Sources
     */
    protected $sources = array();
	
    /**
     * Checks if the user's IP is an authorized IP.
     * 
     * The check is not performed if the user is localhost or if it has already been carried out in the same session.
     * @return boolean TRUE if the user is authorized, otherwise FALSE
	 * @uses config to get the configuration
     */
    private function __checkIp() {
        $ip = $this->request->clientIp(TRUE);
        $allowed = $this->config['security']['ipAllowed'];
        $denied = $this->config['security']['ipDenied'];

        //Skips if the control if the user is localhost or if has already happened and was successful
        if($ip == '127.0.0.1' || $this->Session->read('allowed'))
            return TRUE;

        if(!empty($allowed) && is_string($allowed)) {
            //Turns into a proper regex
            $allowed = str_replace(array(' ', ',', '\*'), array('', '|', '[0-9]{1,3}'), preg_quote($allowed));

            //If the IP doesn't match with any of allowed IPs, exit with error
            if(!preg_match('/^('.$allowed.')$/', $ip))
                return FALSE;
        }
        elseif(!empty($denied) && is_string($denied)) {
            //Turns into a proper regex
            $denied = str_replace(array(' ', ',', '\*'), array('', '|', '[0-9]{1,3}'), preg_quote($denied));

            //If the IP matches with any of the denied IP, exit with error
            if(preg_match('/^('.$denied.')$/', $ip))
                return FALSE;
        }

        //In any other case, saves the result in the session
        $this->Session->write('allowed', TRUE);
        return TRUE;
    }
	
	/**
	 * Gets all sources, without making any control.
     * @return mixed Sources list as array
	 * @uses __getSourcesFile() to get the sources file path
	 */
	protected function __getAllSources() {
		$sources = $this->Xml->getAsArray($this->__getSourcesFile());
		
        if(empty($sources))
            return FALSE;
		
        //Cleans the sources list
        array_walk($sources, function(&$sources, $type) {
			//If is not an array, turns it into an array
			$sources = is_array($sources) ? $sources : array(1 => $sources);
			
            //If is not a valid source type, sets it to FALSE
			$sources = in_array($type, array('audio', 'image', 'video')) ? $sources : FALSE;
			
			//Sets the first key to 1
			$sources = empty($sources) ? FALSE : array_combine(range(1, count($sources)), $sources);
		});
		
		return array_filter($sources);
	}

	/**
     * Gets the configuration and writes it in the cache.
     * @return array Configuration
     * @uses default to get the default configuration
	 * @uses __getConfigFile() to get the configuration file path
     */
    private function __getConfig() {
        if($config = Cache::read('config'))
            return $config;

        $config = $this->Xml->getAsArray($this->__getConfigFile());

        if(!empty($config) && is_array($config)) {
            //Turns "true" and "false" strings into boolean
            array_walk_recursive($config, function(&$v) {
                $v = strtolower($v) === 'true' ? TRUE : (strtolower($v) === 'false' ? FALSE : $v);
            });

            //Replaces missing values with default values
            $config = array_replace_recursive($this->default, $config);
        }
        else
            $config = $this->default;

        //Turns/sets some values
        $config['player']['volume'] = is_numeric($volume = $config['player']['volume']) && preg_match('/^[0-9]+$/', $volume) && $volume >= 0 && $volume <= 100 ? $volume : '100';
        $config['player']['type'] = in_array($player = $config['player']['type'], array('jplayer', 'native')) ? $player : 'jplayer';
        $config['video']['width'] = is_numeric($width = $config['video']['width']) && preg_match('/^[0-9]+$/', $width) && $width >= 240 ? $width : 'auto';

        Cache::write('config', $config);

        return $config;
    }
	
	/**
	 * Gets the path of the configuration file.
	 * @return string Path of the configuration file
	 */
	protected function __getConfigFile() {
		return is_readable($etc = '/etc/ssmp/ssmp.xml') ? $etc : APP.'Config'.DS.'ssmp.xml';
	}

    /**
     * Gets the interface language and writes it in the session.
     * 
     * If a language has been specified and is supported, returns the specified language, otherwise returns english (`eng`)
     * @return string Specified language, if is supported, otherwise english
     * @uses config to get the configuration
     */
    private function __getLanguage() {
        if($this->Session->check('Config.language'))
            return $this->Session->read('Config.language');
		
		$lang = (string) $this->config['app']['language'];
		
		if($lang == 'auto')
			switch(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)) {
				case 'it':
					$lang = 'ita';
					break;
				default:
					$lang = 'eng';
			}
		
		$lang = file_exists(APP.'Locale'.DS.$lang.DS.'LC_MESSAGES'.DS.'default.po') ? $lang : 'eng';

        $this->Session->write('Config.language', $lang);
        return $lang;
    }

	/**
     * Gets the request data (type, source path, relative path, filename and the full path).
     * @return mixed Request data as array or FALSE if the request is not valid
     * @uses sources to get the list of valid sources
	 * @throws NotFoundException
	 */
    protected function __getRequest() {
        $pass = $this->request->pass;

        //Checks if there is a valid source type
        if(empty($pass[0]) || !in_array($pass[0], array('audio', 'image', 'video')))
            throw new NotFoundException(__('Invalid request, no media type specified'));

        //Sets the source type
        $type = $pass[0];
        unset($pass[0]);

        //Checks and sets the source path
        if(!empty($pass[1])) {
            if(!is_numeric($pass[1]) || empty($this->sources[$type][$pass[1]]))
                return FALSE;

            //For now, the source is the full path
            $source = $full_path = $this->sources[$type][$id = $pass[1]];
            unset($pass[1]);
        }

        //Checks and sets the relative path and the filename
        if(!empty($source) && !empty($pass)) {
            //Sets relative and full path
            $full_path = $source.($path = implode(DS, $pass));

            //Checks if is a valid file/directory
            if(!is_file($full_path) && !is_dir($full_path))
                return FALSE;

            //If is a file, sets the filename and the relative path
            if(is_file($full_path)) {
                $filename = pathinfo($path, PATHINFO_BASENAME);
                $path = pathinfo($path, PATHINFO_DIRNAME);
            }
        }

        //Returns request data. Missing data will be set to NULL
        return array(
           'type'       => $type,
           'source'     => empty($source) ? NULL : $source,
           'id'         => empty($id) ? NULL : $id,
           'path'       => empty($path) || $path == '.' ? NULL : $path,
           'filename'   => empty($filename) ? NULL : $filename,
           'full_path'  => empty($full_path) ? NULL : $full_path
        );
    }

    /**
     * Gets all the valid sources and saves it in the cache.
     * @return mixed Sources list as array or FALSE if the sources list is not valid
     * @uses __getAllSources() to get all sources
     */
    private function __getSources() {
        if($sources = Cache::read('sources'))
            return $sources;
		
		$sources = $this->__getAllSources();
		
        if(empty($sources) || !is_array($sources))
            return FALSE;

        //Cleans the sources list
        array_walk($sources, function(&$sources, $type) {
			//Checks if the source exist
			$sources = array_filter(array_map(function($source) {
				if(empty($source))
					return FALSE;
				elseif(!file_exists($source = Folder::slashTerm($source)) || !is_dir($source))
					return FALSE;
				else
					return $source;
			}, $sources));

			//Sets the first key to 1
			$sources = empty($sources) ? FALSE : array_combine(range(1, count($sources)), $sources);
        });
		
		$sources = array_filter($sources);

        //Checks again, in the case that all these sources are not valid
        if(empty($sources))
            return FALSE;
		
        Cache::write('sources', $sources);

        return $sources;
    }
	
	/**
	 * Gets the path of the sources file
	 * @return string Path of the sources file
	 */
	protected function __getSourcesFile() {
		return is_readable($etc = '/etc/ssmp/sources.xml') ? $etc : APP.'Config'.DS.'sources.xml';
	}

	/**
     * Called before the controller action.
	 * @throws ForbiddenException
	 * @uses action to set the current action name
     * @uses config to set the configuration
	 * @uses controller to set the current controller name
     * @uses sources to set the sources list
     * @uses __checkIp() to check if the user's IP is allowed
     * @uses __getConfig() to get the configuration
     * @uses __getLanguange() to get the languange
     * @uses __getSources() to get the list of valid sources
	 */
    public function beforeFilter() {
		$this->action = $this->request->params['action'];
		$this->controller = $this->request->params['controller'];
		
        //Sets the configuration
        $this->config = $this->__getConfig();
	
        //Sets the language
        Configure::write('Config.language', $this->__getLanguage());

        //Checks for user's IP
        if(!$this->__checkIp())
            throw new ForbiddenException(__('Your IP address is not allowed'));
		
        //Sets the sources list
        $this->sources = $this->__getSources();
		
		//If there are no sources and the controller is not "pages" (except for "homepage" action) OR "tools"
		if(empty($this->sources) && (!in_array($this->controller, array('pages', 'tools')) || $this->action == 'homepage')) {
			//Sets the current url as referer, then redirects
			$this->Session->write('App.referer', Router::url(NULL, TRUE));
			$this->redirect(array('controller' => 'tools', 'action' => 'no_sources'));
		}
    }

    /**
     * Called after the controller action is run, but before the view is rendered.
     * @uses config to get the configuration
	 * @uses data to get the request data
     * @uses sources to get the list of valid sources
     */
    public function beforeRender() {
        //Turns/sets some config values before passing them to the view
		$config = $this->config;
		$config['player']['type'] = $config['player']['nativeOnMobile'] && ($isMobile = $this->request->is('mobile')) ? 'native' : $config['player']['type'];
		$config['player']['volume'] = $config['player']['volume'] / 100;
		$config['video']['width'] = is_numeric($width = $config['video']['width']) ? sprintf('%spx', $width) : $width;
		
        $this->set(array(
            'all_sources'   => $this->sources,
            'config'        => $config,
            'data'          => empty($this->data) ? array() : $this->data,
            'isMobile'      => $isMobile
        ));
    }
}
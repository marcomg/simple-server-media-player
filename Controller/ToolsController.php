<?php

/**
 * ToolsController.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\Controller
 */
App::uses('AppController', 'Controller');
App::uses('System', 'MeTools.Utility');

/**
 * It provides some tools for the system management
 */
class ToolsController extends AppController {
    /**
     * Checks if the playlists directories and files are readable and writable
     * @return boolean TRUE if if the playlists directories and files are readable and writable, FALSE otherwise
     */
    private function __checkPlaylists() {
        $playlists = new Folder(TMP.'playlists');
        $playlists = $playlists->tree(TMP.'playlists', array('empty', '.'));
        $playlists = am($playlists[0], $playlists[1]);

        foreach($playlists as $file) {
            if(!is_readable($file) || !is_writable($file))
                return FALSE;
        }

        return TRUE;
    }
	
	/**
	 * Gets the SSMP number version.
	 * @return string SSMP number version
	 */
	private function __getVersion() {
		return @file_get_contents(APP . 'version');
	}
	
	/**
	 * Adds a new source.
	 * @uses __getAllSources() to get all the sources
	 * @uses __getSourcesFile() to get the sources file path
	 */
	public function add_source() {
		//If the sources file is not writable
		if(!is_writable($file = $this->__getSourcesFile())) {
			$this->Session->flash(__('You cannot add new sources, because the file <em>%s</em> is not writable', $file), 'error');
			$this->redirect(array('action' => 'sources'));
		}
		
		if($this->request->is('post')) {
			//Loads and sets the Source model
			$this->loadModel('Source');
			$this->Source->set($this->request->data);
			
			if($this->Source->validates()) {
				$sources = is_array($sources = $this->__getAllSources()) ? $sources : array();
				$sources[$this->request->data['Source']['type']][] = $this->request->data['Source']['path'];
				
				//Removes duplicates
				$sources = array_map(function($sources) {
					return array_unique($sources);
				}, $sources);
				
				$file = new File($file);
				if($file->write($this->Xml->toXml(array('sources' => $sources)))) {
					Cache::delete('sources');
					$this->Session->flash(__('The new source has been added'), 'success');
					$this->redirect(array('action' => 'sources'));
				}
			}
		}
	}
	
	/**
	 * Provides informations about the cache and thumbs.
	 */
    public function cache() {
        $this->set(array(
           'cacheStatus'    => System::checkCacheStatus(),
           'cacheSize'      => CakeNumber::toReadableSize(System::getCacheSize()),
           'thumbsSize'     => CakeNumber::toReadableSize(System::getThumbsSize())
        ));
    }

    /**
	 * Performs a check up of the system.
	 * @uses __checkPlaylists() to check if the playlists are readable and writable
	 * @uses __getVersion() to get the SSMP version
	 */
	public function checkup() {
		$phpRequired = '5.2.8';

		$this->set(array(
			'cache'				=> System::checkCache(),
			'cacheStatus'		=> System::checkCacheStatus(),
			'cakeVersion'		=> System::getCakeVersion(),
			'exif'				=> System::checkPhpExtension('exif'),
			'expires'			=> System::checkApacheModule('mod_expires'),
			'fileinfo'			=> System::checkPhpExtension('fileinfo'),
			'gd'				=> System::checkPhpExtension('gd'),
			'metoolsVersion'	=> System::getMeToolsVersion(),
			'phpRequired'		=> $phpRequired,
			'phpVersion'		=> System::checkPhpVersion($phpRequired),
			'playlists'			=> $this->__checkPlaylists(),
			'rewrite'			=> System::checkApacheModule('mod_rewrite'),
			'ssmpVersion'		=> $this->__getVersion(),
			'tmp'				=> System::checkTmp(),
			'thumbs'			=> System::checkThumbs()
		));
	}
	
	/**
	 * Clear the cache.
	 */
	public function clearCache() {
		$this->request->onlyAllow('post', 'delete'); 
		
		if(System::clearCache())
			$this->Session->flash(__('The cache has been cleared'), 'success');
		else
			$this->Session->flash(__('The cache has not been cleared'), 'error');
		
		$this->redirect(array('action' => 'cache'));
		
	}
	
	/**
	 * Clear the thumbs.
	 */
	public function clearThumbs() {
		$this->request->onlyAllow('post', 'delete');
		
		if(System::clearThumbs())
			$this->Session->flash(__('Thumbnails have been deleted'), 'success');
		else
			$this->Session->flash(__('Thumbnails have not been deleted'), 'error');
		
		$this->redirect(array('action' => 'cache'));
	}
	
	/**
	 * Edits the configuration.
	 * @uses __getConfigFile() to get the configuration file path
	 */
	public function configuration() {
		//If the config file is not writable
		if(!is_writable($file = $this->__getConfigFile())) {
			$this->Session->flash(__('You cannot edit configuration, because the file <em>%s</em> is not writable', $file), 'error');
			$this->redirect('/');
		}
			
		if($this->request->is('post')) {
			//Loads and sets the Config model
			$this->loadModel('Configuration');
			$this->Configuration->set($this->request->data);
			
			if($this->Configuration->validates()) {
				$config = array();
				foreach($this->request->data['Configuration'] as $key => $value) {
					$key = explode('_', $key);
					$config[$key[0]][$key[1]] = $value;
				}
				
				$file = new File($file);
				if($file->write($this->Xml->toXml(array('configuration' => $config)))) {
					Cache::delete('config');
					$this->Session->flash(__('The configuration has been saved'), 'success');
					$this->redirect('/');
				}
			}
		}
		else {
			$configTmp = array();
			foreach($this->config as $section => $config)
				foreach($config as $key => $value)
					$configTmp[$section.'_'.$key] = $value;
			
			$this->request->data['Configuration'] = $configTmp;
		}
	}
	
	/**
	 * Deletes a source..
	 * @param string $type Source type
	 * @param string $id Source id
	 * @uses __getAllSources() to get all sources
	 * @uses __getSourcesFile() to get the sources file path
	 */
	public function delete_source($type, $id) {
		$this->request->onlyAllow('post', 'delete');
		
		//If the sources file is not writable
		if(!is_writable($file = $this->__getSourcesFile()))
			$this->Session->flash(__('You cannot delete sources, because the file <em>%s</em> is not writable', $file), 'error');
		
		$sources = $this->__getAllSources();
		
		//If the source that you want to delete does not exist
		if(empty($sources[$type][$id]))
			$this->Session->flash(__('You cannot delete this source'), 'error');
		
		unset($sources[$type][$id]);
				
		$file = new File($file);
		if($file->write($this->Xml->toXml(array('sources' => array_filter($sources))))) {
			Cache::delete('sources');
			$this->Session->flash(__('The source has been deleted'), 'success');
		}
		
		$this->redirect(array('action' => 'sources'));
	}
	
	/**
	 * "No sources" page.
	 * @uses __getSourcesFile() to get the sources file path
	 */
	public function no_sources() {
		//Gets and sets the referer url
		if($this->Session->check('App.referer'))
			$this->set('referer', $this->Session->read('App.referer'));
		
		$this->set(array('file' => $this->__getSourcesFile()));
	}

	/**
	 * Manages sources.
	 * @uses __getAllSources() to get all sources
	 */
	public function sources() {
		$sources = $this->__getAllSources();
		
		if(!empty($sources))
			$sources = array_filter(array_map(function($sources) {
				return array_filter(array_map(function($source) {
					if(empty($source))
						return FALSE;
					
					//Checks if exists
					if(!file_exists($source))
						$check = 'not_exists';
					//Checks if is readable
					elseif(!is_readable($source))
						$check = 'not_readable';
					//Checks if is a directory
					elseif(!is_dir($source))
						$check = 'not_dir';
					//Checks if is empty (this should be the last control, because the control is still valid)
					elseif(count(scandir($source)) <= 2)
						$check = 'is_empty';
					else
						$check = 'valid';

					return compact('source', 'check');
				}, $sources));
			}, $sources));
		
		$this->set(compact('sources'));
	}

    /**
     * Checks for SSMP updates.
	 * @uses __getVersion() to get the SSMP version
     */
    public function updates() {
        //Gets the current version
        $current = $this->__getVersion();

        //Gets the latest version
        if($latest = @file_get_contents('https://bitbucket.org/api/1.0/repositories/nova-atlantis/simple-server-media-player/raw/master/version'))
            $latest = array('link' => 'http://ssmp.novatlantis.it', 'version' => str_replace(array("\n", "\r"), '', $latest));
        else
            $latest = $this->Xml->getAsArray('http://repository.novatlantis.it/ssmp/last-version.xml');

        if(!empty($latest) && !empty($current)) {
            //Checks for new version. `$newVersionAvailable` will be TRUE if a new version is available
            $newVersionAvailable = version_compare($latest['version'], $current, '>');

            $this->set(compact('newVersionAvailable', 'latest'));
        }
    }
}
<?php
/**
 * Default layout.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Layouts
 */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			echo $this->Html->charset();
			echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'));
			echo $this->Html->meta('icon');
			echo $this->fetch('meta');
			echo $this->Html->css('libraries', array('inline' => TRUE));
			echo $this->fetch('css');
			echo $this->Html->js('libraries', array('inline' => TRUE));
			echo $this->fetch('script');
		?>
		<title>Simple server media player</title>
	</head>
	<body>
		<?php echo $this->element('topbar'); ?>
		<div class="container">
			<?php
				if($config['app']['logo'])
					echo $this->Html->img('logo.png', array('id' => 'logo', 'class' => 'hidden-xs'));

				if($config['app']['fortune'])
					echo $this->element('quotes');
			?>
			<div id="content">
				<?php
					echo $this->Session->flash(); 
					echo $this->fetch('content'); 
				?>
			</div>
			<div id="footer"></div>
		</div>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>
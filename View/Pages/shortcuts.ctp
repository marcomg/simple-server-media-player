<?php
/**
 * Shortcuts page.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Pages
 */
?>

<?php 
	echo $this->Html->tag('h3', __('Shortcuts').$this->Html->tag('small', __('Keyboard shortcuts')));

	echo $this->Html->para(NULL, __('Using %s as media player, you can use keyboard shortcuts to control the player. Here you can find the function of each key.', 'jPlayer'));

	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', __('Space')).__('Play/pause')); 
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', __('Enter')).sprintf('%s <sup>[1]</sup>', __('Full screen'))); 
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', __('Backspace')).__('Muted/unmuted'));
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', $this->Html->icon('arrow-up')).__('More volume')); 
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', $this->Html->icon('arrow-down')).__('Less volume')); 
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', $this->Html->icon('arrow-right')).sprintf('%s <sup>[2]</sup>', __('Next track'))); 
	echo $this->Html->para(NULL, $this->Html->div('keyboard-btn', $this->Html->icon('arrow-left')).sprintf('%s <sup>[2]</sup>', __('Previous track')));

	echo $this->Html->div('small', sprintf('<sup>[1]</sup> %s', __('This works only with video files')));
	echo $this->Html->div('small', sprintf('<sup>[2]</sup> %s', __('This works only with playlists')));
?>
<?php
/**
 * Homepage.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Pages
 */
?>

<?php
	$links = array();
			
	if(!empty($all_sources['audio']))
		$links[] = $this->Html->link($this->Html->tag('span', __('Audio'), array('class' => 'link-title')), array('controller' => 'browser', 'action' => 'sources', 'type' => 'audio'), array('icon' => 'music'));
	else
		$links[] = $this->Html->link(NULL, '#', array('class' => 'disabled', 'icon' => 'music'));

	if(!empty($all_sources['image']))
		$links[] = $this->Html->link($this->Html->tag('span', __('Images'), array('class' => 'link-title')), array('controller' => 'browser', 'action' => 'sources', 'type' => 'image'), array('icon' => 'camera-retro'));
	else
		$links[] = $this->Html->link(NULL, '#', array('class' => 'disabled', 'icon' => 'camera-retro'));

	if(!empty($all_sources['video']))
		$links[] = $this->Html->link($this->Html->tag('span', __('Video'), array('class' => 'link-title')), array('controller' => 'browser', 'action' => 'sources', 'type' => 'video'), array('icon' => 'film'));
	else
		$links[] = $this->Html->link(NULL, '#', array('class' => 'disabled', 'icon' => 'film'));

	$links[] = $this->Html->link($this->Html->tag('span', __('Search'), array('class' => 'link-title')), array('controller' => 'browser', 'action' => 'search'), array('icon' => 'search', 'id' => 'show-search-form'));

	$html = $this->element('search');

	foreach($links as $link)
		$html .= $this->Html->div('col-xs-6 col-md-3', $link);

	echo $this->Html->div(NULL, $this->Html->div('icons', $this->Html->div('row', $html)), array('id' => 'homepage'));
?>
<?php
/**
 * About page.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Pages
 */
?>

<?php
	echo $this->Html->tag('h3', __('About').$this->Html->tag('small', __('About %s', 'SSMP')));

	echo $this->Html->para(NULL, __('%s (%s) is a (simple) web application for playing media contents (such as audio, images and videos) located on a server.', $this->Html->tag('em' , 'Simple server media player'), 'SSMP'));
	echo $this->Html->para(NULL, __('It was developed by %s for %s and is written in PHP and Javascript, using the %s framework.',
			$this->Html->link('Mirko Pagliai', 'http://mirkopagliai.it', array('target' => '_blank')),
			'Nova Atlantis LTD',
			$this->Html->link('CakePHP', 'http://cakephp.org', array('target' => '_blank'))));
	
	echo $this->Html->tag('h4', __('Support'));
	echo $this->Html->para(NULL, __('If you need support for %s, please visit %s and look at the %s.', 'SSMP',
			$this->Html->link(__('our website'), 'http://ssmp.novatlantis.it', array('target' => '_blank')),
			$this->Html->link(__('support page'), 'http://ssmp.novatlantis.it/support', array('target' => '_blank'))));
	echo $this->Html->para(NULL, __('Instead, if you have issues playing an audio or video file, see the %s and make sure your browser has support for HTML5 and codecs to play the file. In fact, only some browsers can play all file formats supported by %s.',
			$this->Html->link(__('compatibility table'), 'http://ssmp.novatlantis.it/compatibility', array('target' => '_blank')),
			'SSMP'));
	
	echo $this->Html->tag('h4', __('Sources'));
	echo $this->Html->para(NULL, __('%s sources are freely available from %s.', 'SSMP',
			$this->Html->link(__('our git repository'), 'http://git.novatlantis.it/simple-server-media-player', array('target' => '_blank'))));
	
	echo $this->Html->tag('h4', __('Changelog'));
	echo $this->Html->para(NULL, __('You can find the changelog %s.',
			$this->Html->link(__('here'), 'http://ssmp.novatlantis.it/changelog', array('target' => '_blank'))));
	
	echo $this->Html->tag('h4', __('License'));
	echo $this->Html->para(NULL, __('%s is licensed under the %s.', 'SSMP',
			$this->Html->link(__('%s license', 'AGPL'), 'http://www.gnu.org/licenses/agpl.html', array('target' => '_blank'))));
	echo $this->Html->para(NULL, __('You can find the full text of the license %s and some translations %s.',
			$this->Html->link(__('here'), 'http://www.gnu.org/licenses/agpl-3.0-standalone.html', array('target' => '_blank')),
			$this->Html->link(__('here'), 'http://www.gnu.org/licenses/translations.html', array('target' => '_blank'))));
	
	echo $this->Html->tag('h4', __('Translations'));
	echo $this->Html->para(NULL, __('Thanks to %s for the italian translation.', 
			$this->Html->link('Marco Guerrini', 'https://bitbucket.org/marcomg', array('target' => '_blank'))));

	echo $this->Html->tag('h4', 'Easter egg');
	echo $this->Html->para(NULL, __('Yes, %s contains an %s. %s was the first to find it.', 'SSMP', 'easter egg', 'Caig'));
	
	echo $this->Html->tag('h4', __('Libraries, icons, etc'));
	echo $this->Html->para(NULL, __('To develop %s, these libraries have been used:', 'SSMP'));
	echo $this->Html->ul(array(
		__('%s, a PHP framework for web applications;', $this->Html->link('CakePHP', 'http://cakephp.org', array('target' => '_blank'))),
		__('%s, a CakePHP plugin to improve development;', $this->Html->link('MeTools', 'http://git.novatlantis.it/metools', array('target' => '_blank'))),
		__('%s, a JavaScript library;', $this->Html->link('jQuery', 'http://jquery.com', array('target' => '_blank'))),
		__('%s, a front-end framework for faster and easier web development;', $this->Html->link('Bootstrap', 'http://twitter.github.com/bootstrap', array('target' => '_blank'))),
		__('%s, an HTML5 audio/video player;', $this->Html->link('jPlayer', 'http://jplayer.org/', array('target' => '_blank'))),
		__('%s, a library to extract Id3 tag from audio and video files;', $this->Html->link('getID3', 'http://getid3.sourceforge.net', array('target' => '_blank'))),
		__('%s icons.', $this->Html->link('Font Awesome', 'http://fortawesome.github.io/Font-Awesome', array('target' => '_blank')))
	), NULL, array('icon' => 'caret-right'));
?>
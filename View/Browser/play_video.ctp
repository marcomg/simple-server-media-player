<?php
/**
 * Plays video files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	echo $this->Html->tag('h3', __('Video').$this->Html->tag('small', $data['source'], array('class' => 'no-wrap')));
	echo $this->element('breadcrumb');
	//Sets the file url. It will be used by the player and for the download link
	$url = $this->Html->url(am(array('action' => 'download', $data['type'], $data['id']), explode(DS, $data['path']), $data['filename']), TRUE);
?>

<div class="media-view">
	<?php 
		if($config['player']['type'] == 'jplayer')
			echo $this->element('jplayer_video', array('url' => $url)); 
		else
			echo $this->Html->div('native-player native-video', $this->Html->video($url, array(
				'autoplay'	=> $config['player']['autoplay'],
				'preload'	=> $config['player']['preload'] ? 'auto' : 'none',
				'text'		=> __('Your browser doesn\'t support the HTML5 video tag'),
				'width'		=> $config['video']['width']
			)));
		
		//Nav tabs
		$list = $this->Html->li($this->Html->link(__('File info'), '#file-info', array('data-toggle' => 'tab', 'icon' => 'tag')), array('class' => 'active'));
		if(!empty($id3['audio']))
			$list .= $this->Html->li($this->Html->link(__('Id3 Video'), '#audio-id3-tags', array('data-toggle' => 'tab', 'icon' => 'music')));
		if(!empty($id3['video']))
			$list .= $this->Html->li($this->Html->link(__('Id3 Video'), '#video-id3-tags', array('data-toggle' => 'tab', 'icon' => 'film'))); 
		echo $this->Html->tag('ul', $list, array('class' => 'nav nav-tabs'));
	?>

	<!-- Tab panes -->
	<div class="tab-content tab-content-base">
		<div class="tab-pane fade in active" id="file-info">
			<dl class="dl-horizontal">
				<?php
					echo $this->Html->tag('dt', __('Name'));
					echo $this->Html->tag('dd', $info['basename']);
					echo $this->Html->tag('dt', __('Directory'));
					echo $this->Html->tag('dd', $info['dirname']);
					echo $this->Html->tag('dt', __('Size'));
					echo $this->Html->tag('dd', $info['filesize']);
					echo $this->Html->tag('dt', __('Type'));
					echo $this->Html->tag('dd', $info['mime']);
				?>
			</dl>
		</div>
		<?php if(!empty($id3['audio'])): ?>
			<div class="tab-pane fade" id="audio-id3-tags">
				<dl class="dl-horizontal">
					<?php
						if(!empty($id3['audio']['dataformat'])) {
							echo $this->Html->tag('dt', __('Format'));
							echo $this->Html->tag('dd', $id3['audio']['dataformat']);
						}
						if(!empty($id3['audio']['codec'])) {
							echo $this->Html->tag('dt', __('Codec'));
							echo $this->Html->tag('dd', $id3['audio']['codec']);
						}
						if(!empty($id3['audio']['sample_rate'])) {
							echo $this->Html->tag('dt', __('Sample rate'));
							echo $this->Html->tag('dd', $id3['audio']['sample_rate']);
						}
						if(!empty($id3['audio']['bitrate'])) {
							echo $this->Html->tag('dt', __('Bitrate'));
							echo $this->Html->tag('dd', $id3['audio']['bitrate']);
						}
						if(!empty($id3['audio']['language'])) {
							echo $this->Html->tag('dt', __('Language'));
							echo $this->Html->tag('dd', $id3['audio']['language']);
						}
						if(!empty($id3['audio']['channels'])) {
							echo $this->Html->tag('dt', __('Channels'));
							echo $this->Html->tag('dd', $id3['audio']['channels']);
						}
						if(!empty($id3['audio']['channelmode'])) {
							echo $this->Html->tag('dt', __('Channel mode'));
							echo $this->Html->tag('dd', $id3['audio']['channelmode']);
						}
						if(!empty($id3['audio']['bits_per_sample'])) {
							echo $this->Html->tag('dt', __('Bit depth'));
							echo $this->Html->tag('dd', $id3['audio']['bits_per_sample']);
						}
						if(!empty($id3['playtime'])) {
							echo $this->Html->tag('dt', __('Playtime'));
							echo $this->Html->tag('dd', $id3['playtime']);
						}
					?>
				</dl>
			</div>
		<?php endif; ?>
		<?php if(!empty($id3['video'])): ?>
			<div class="tab-pane fade" id="video-id3-tags">
				<dl class="dl-horizontal">
					<?php
						if(!empty($id3['video']['dataformat'])) {
							echo $this->Html->tag('dt', __('Format'));
							echo $this->Html->tag('dd', $id3['video']['dataformat']);
						}
						if(!empty($id3['video']['resolution_x']) && !empty($id3['video']['resolution_y'])) {
							echo $this->Html->tag('dt', __('Resolution'));
							echo $this->Html->tag('dd', sprintf('%sx%s px', $id3['video']['resolution_x'], $id3['video']['resolution_y']));
						}
						if(!empty($id3['video']['bitrate'])) {
							echo $this->Html->tag('dt', __('Bitrate'));
							echo $this->Html->tag('dd', $id3['video']['bitrate']);
						}
						if(!empty($id3['video']['frame_rate'])) {
							echo $this->Html->tag('dt', __('Frame rate'));
							echo $this->Html->tag('dd', $id3['video']['frame_rate']);
						}
						if(!empty($id3['playtime'])) {
							echo $this->Html->tag('dt', __('Playtime'));
							echo $this->Html->tag('dd', $id3['playtime']);
						}
					?>
				</dl>
			</div>
		<?php endif; ?>
	</div>
	
	<?php echo $this->Html->linkButton(__('Download'), $url, array('class' => 'btn-primary', 'icon' => 'download')); ?>
</div>
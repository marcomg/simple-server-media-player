<?php
/**
 * Lists sources.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	$title = $data['type'] == 'audio' ? __('Audio') : ($data['type'] == 'image' ? __('Images') : __('Video'));
	
	echo $this->Html->tag('h3', $title.$this->Html->tag('small', __('Select source'), array('class' => 'no-wrap')));

	if(!empty($sources)) {
		$html = NULL;
		foreach($sources as $id => $source)
			$html .= $this->Html->link($source, array('action' => 'browse', 'type' => $data['type'], 'id' => $id), array('class' => 'list-group-item no-wrap', 'icon' => 'folder'));

		echo $this->Html->div('browser-view list-group', $html);
	}
?>
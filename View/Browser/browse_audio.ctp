<?php
/**
 * Browses audio directories and files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
    echo $this->Html->tag('h3', __('Audio').$this->Html->tag('small', $data['source'], array('class' => 'no-wrap')));
    echo $this->element('breadcrumb');

    //If there's more than one file, shows the form to add a media to a playlist and allows to play the folder as a playlist
    if(!empty($files)) {
        echo $this->element('add_to_playlist');

        $url = $this->Html->url(am(array('controller' => 'playlists', 'action' => 'play_as_dir', $data['type'], $data['id']), explode(DS, $data['path'])), TRUE);
        $html = $this->Html->button(__('Play folder'), $url, array('class' => 'btn-primary', 'icon' => 'play'));
        $html .= $this->Html->button(__('Add to playlist'), '#', array('class' => 'btn-primary', 'icon' => 'plus', 'onclick' => 'showAddToPlaylist(event)'));
        echo $this->Html->div('btn-group display-buttons pull-right', $html);
    }

    if(!empty($dirs) || !empty($files)) {
        $html = NULL;

        if(!empty($dirs))
            foreach($dirs as $dir) {
                $action = am(array('type' => $data['type'], 'id' => $data['id']), explode(DS, $data['path']), $dir);
                $html .= $this->Html->link($dir, $action, array('class' => 'list-group-item no-wrap', 'icon' => 'folder'));
            }

        if(!empty($files))
            foreach($files as $file) {
                $url = $this->Html->url(am(array('controller' => 'playlists', 'action' => 'addToPlaylistAPI', $data['type'], $data['id']), explode(DS, $data['path']), $file));
                $html .= $this->Html->link(NULL, '#', array('class' => 'add-to-playlist-btn', 'data-url' => $url, 'icon' => 'plus', 'onclick' => 'addToPlaylist(this, event)'));
                $action = am(array('action' => 'play', $data['type'], $data['id']), explode(DS, $data['path']), $file);
                $html .= $this->Html->link($file, $action, array('class' => 'list-group-item no-wrap', 'icon' => 'music'));
            }

        echo $this->Html->div('browser-view list-group', $html);
    }
?>
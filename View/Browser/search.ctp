<?php
/**
 * Searches files through the file system.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	echo $this->Html->tag('h3', __('Search').$this->Html->tag('small', __('Search media files')));
	echo $this->element('search', array('options' => array('inline' => FALSE)));
	
	if(!empty($results)) {
		$icon = $type == 'audio' ? 'music' : ($type == 'image' ? 'camera-retro' : 'film');
		
		foreach($results as $result) {
			echo $this->Html->tag('h4', $result['source'].$this->Html->tag('small', sprintf('%s: %s', __('Results for this source'), count($result['files'])), array('class' => 'no-wrap')), array('class' => 'no-wrap'));
			
			//Gets the source ID
			$id = array_search($result['source'], $all_sources[$type]);
			
			$html = NULL;
			foreach($result['files'] as $file)
				$html .= $this->Html->link($file['filename'], am(array('action' => 'play', $type, $id), explode(DS, $file['path']), $file['filename']), array('class' => 'list-group-item no-wrap', 'icon' => $icon));
			
			echo $this->Html->div('browser-view list-group', $html);
		}
	}
	
	if(!empty($exec_time))
		echo $this->Html->para('small text-muted text-center', __('Search was performed in %s seconds', $this->Number->precision($exec_time)));
?>
<?php
/**
 * Plays audio files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	echo $this->Html->tag('h3', __('Audio').$this->Html->tag('small', $data['source'], array('class' => 'no-wrap')));
	echo $this->element('breadcrumb');
	//Sets the file url. It will be used by the player and for the download link
	$url = $this->Html->url(am(array('action' => 'download', $data['type'], $data['id']), explode(DS, $data['path']), $data['filename']), TRUE);
?>

<div class="media-view">
	<?php 
		if($config['player']['type'] == 'jplayer')
			echo $this->element('jplayer_audio', array('url' => $url));
		else {
			$html = NULL;
			if($albumArt)
				$html .= $this->Html->thumb($albumArt, array('class' => 'album-art', 'side' => '200'));
			$html .= $this->Html->audio($url, array(
				'autoplay'	=> $config['player']['autoplay'], 
				'preload'	=> $config['player']['preload'] ? 'auto' : 'none', 
				'text'		=> __('Your browser doesn\'t support the HTML5 audio tag')
			));
			echo $this->Html->div('native-player native-audio', $html);
		}
		
		//Nav tabs
		$list = $this->Html->li($this->Html->link(__('File info'), '#file-info', array('data-toggle' => 'tab', 'icon' => 'tag')), array('class' => 'active'));
		if(!empty($id3))
			$list .= $this->Html->li($this->Html->link(__('Id3 Audio'), '#audio-id3-tags', array('data-toggle' => 'tab', 'icon' => 'music')));
		echo $this->Html->tag('ul', $list, array('class' => 'nav nav-tabs'));
	?>

	<!-- Tab panes -->
	<div class="tab-content tab-content-base">
		<div class="tab-pane fade in active" id="file-info">
			<dl class="dl-horizontal">
				<?php
					echo $this->Html->tag('dt', __('Name'));
					echo $this->Html->tag('dd', $info['basename']);
					echo $this->Html->tag('dt', __('Directory'));
					echo $this->Html->tag('dd', $info['dirname']);
					echo $this->Html->tag('dt', __('Size'));
					echo $this->Html->tag('dd', $info['filesize']);
					echo $this->Html->tag('dt', __('Type'));
					echo $this->Html->tag('dd', $info['mime']);
				?>
			</dl>
		</div>
		<?php if(!empty($id3)): ?>
			<div class="tab-pane fade" id="audio-id3-tags">
				<dl class="dl-horizontal">
					<?php
						if(!empty($id3['title'])) {
							echo $this->Html->tag('dt', __('Title'));
							echo $this->Html->tag('dd', $id3['title']);
						}
						if(!empty($id3['album'])) {
							echo $this->Html->tag('dt', __('Album'));
							echo $this->Html->tag('dd', $id3['album']);
						}
						if(!empty($id3['artist'])) {
							echo $this->Html->tag('dt', __('Artist'));
							echo $this->Html->tag('dd', $id3['artist']);
						}
						if(!empty($id3['track_number'])) {
							echo $this->Html->tag('dt', __('Track'));
							echo $this->Html->tag('dd', $id3['track_number']);
						}
						if(!empty($id3['discnumber']) || !empty($id3['part_of_a_set'])) {
							echo $this->Html->tag('dt', __('Disk'));
							echo $this->Html->tag('dd', empty($id3['discnumber']) ? $id3['part_of_a_set'] : $id3['discnumber']);
						}
						if(!empty($id3['year']) || !empty($id3['recording_time'])) {
							echo $this->Html->tag('dt', __('Year'));
							echo $this->Html->tag('dd', empty($id3['year']) ? $id3['recording_time'] : $id3['year']);
						}
						if(!empty($id3['fileformat'])) {
							echo $this->Html->tag('dt', __('Format'));
							echo $this->Html->tag('dd', $id3['fileformat']);
						}
						if(!empty($id3['codec'])) {
							echo $this->Html->tag('dt', __('Codec'));
							echo $this->Html->tag('dd', $id3['codec']);
						}
						if(!empty($id3['encoder'])) {
							echo $this->Html->tag('dt', __('Encoder'));
							echo $this->Html->tag('dd', $id3['encoder']);
						}
						if(!empty($id3['sample_rate'])) {
							echo $this->Html->tag('dt', __('Sample rate'));
							echo $this->Html->tag('dd', $id3['sample_rate']);
						}
						if(!empty($id3['channels'])) {
							echo $this->Html->tag('dt', __('Channels'));
							echo $this->Html->tag('dd', $id3['channels']);
						}
						if(!empty($id3['channelmode'])) {
							echo $this->Html->tag('dt', __('Channel mode'));
							echo $this->Html->tag('dd', $id3['channelmode']);
						}
						if(!empty($id3['bitrate'])) {
							echo $this->Html->tag('dt', __('Bitrate'));
							echo $this->Html->tag('dd', $id3['bitrate']);
						}
						if(!empty($id3['playtime'])) {
							echo $this->Html->tag('dt', __('Playtime'));
							echo $this->Html->tag('dd', $id3['playtime']);
						}
						if(!empty($id3['encoder_settings'])) {
							echo $this->Html->tag('dt', __('Encoder'));
							echo $this->Html->tag('dd', $id3['encoder_settings']);
						}
					?>
				</dl>
			</div>
		<?php endif; ?>
	</div>
	
	<?php echo $this->Html->linkButton(__('Download'), $url, array('class' => 'btn-primary', 'icon' => 'download')); ?>
</div>
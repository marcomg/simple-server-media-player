<?php
/**
 * Browses image directories and files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	echo $this->Html->tag('h3',  __('Images').$this->Html->tag('small', $data['source'], array('class' => 'no-wrap')));
	echo $this->element('breadcrumb');
	
	//Sets the url for display images as list/thumbs
	$url = $this->Html->url(am(array($data['type'], $data['id']), explode('/', $data['path'])), TRUE);
	
	if(!empty($files) && isset($this->request->query['thumb']))
		echo $this->Html->div('display-buttons pull-right', $this->Html->linkButton(__('Display as list'), $url, array('class' => 'btn-primary', 'icon' => 'bars')));
	elseif(!empty($files))
		echo $this->Html->div('display-buttons pull-right', $this->Html->linkButton(__('Display as thumbs'), $url.'?thumb', array('class' => 'btn-primary', 'icon' => 'th')));

	$html = NULL;

	//If it was requested to view images as thumbs
	if(isset($this->request->query['thumb']) && !empty($files)) {
		foreach($files as $file) {
			$action = am(array('action' => 'play', $data['type'], $data['id']), explode(DS, $data['path']), array($file));
			$thumb = $this->Html->thumb($data['source'].$data['path'].DS.$file, array('side' => 270));
			$html .= $this->Html->div('col-xs-6 col-sm-4 col-md-3 col-lg-2', $this->Html->link($thumb, $action, array('class' => 'thumbnail')));
		}
		echo $this->Html->div('browser-view thumbs-view', $this->Html->div('row', $html));
	}
	//Else, if it was requested to view images as list
	elseif(!empty($dirs) || !empty($files)) {
		if(!empty($dirs))
			foreach($dirs as $dir) {
				$action = am(array('type' => $data['type'], 'id' => $data['id']), explode(DS, $data['path']), $dir);
				$html .= $this->Html->link($dir, $action, array('class' => 'list-group-item no-wrap', 'icon' => 'folder'));
			}

		if(!empty($files))
			foreach($files as $file) {
				$action = am(array('action' => 'play', $data['type'], $data['id']), explode(DS, $data['path']), $file);
				$html .= $this->Html->link($file, $action, array('class' => 'list-group-item no-wrap', 'icon' => 'camera-retro'));
			}

		echo $this->Html->div('browser-view list-group', $html);
	}
?>
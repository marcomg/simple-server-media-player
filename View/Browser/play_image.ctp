<?php
/**
 * Plays image files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Browser
 */
?>

<?php
	echo $this->Html->tag('h3', __('Images').$this->Html->tag('small', $data['source'], array('class' => 'no-wrap')));
	echo $this->element('breadcrumb');
?>

<div class="media-view">
	<?php 
		echo $this->Html->thumb($data['full_path'], array('class' => 'img-thumbnail', 'width' => 1130));
		
		//Nav tabs
		$list = $this->Html->li($this->Html->link(__('File info'), '#file-info', array('data-toggle' => 'tab', 'icon' => 'tag')), array('class' => 'active'));
		if(!empty($exif))
			$list .= $this->Html->li($this->Html->link(__('Exif data'), '#exif-data', array('data-toggle' => 'tab', 'icon' => 'camera-retro')));
		echo $this->Html->tag('ul', $list, array('class' => 'nav nav-tabs'));
	?>

	<!-- Tab panes -->
	<div class="tab-content tab-content-base">
		<div class="tab-pane fade in active" id="file-info">
			<dl class="dl-horizontal">
				<?php
					echo $this->Html->tag('dt', __('Name'));
					echo $this->Html->tag('dd', $info['basename']);
					echo $this->Html->tag('dt', __('Directory'));
					echo $this->Html->tag('dd', $info['dirname']);
					echo $this->Html->tag('dt', __('Size'));
					echo $this->Html->tag('dd', $info['filesize']);
					echo $this->Html->tag('dt', __('Type'));
					echo $this->Html->tag('dd', $info['mime']);
				?>
			</dl>
		</div>
		<?php if(!empty($exif)): ?>
			<div class="tab-pane fade" id="exif-data">
				<dl class="dl-horizontal">
					<?php
						foreach($exif as $section)
							foreach($section as $key => $value) {
								echo $this->Html->tag('dt', $key);
								echo $this->Html->tag('dd', !is_array($value) ? $value : implode(', ', $value));
							}
					?>
				</dl>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$url = $this->Html->url(am(array('action' => 'download', $data['type'], $data['id']), explode(DS, $data['path']), $data['filename']), TRUE);
		echo $this->Html->linkButton(__('Download'), $url, array('class' => 'btn-primary', 'icon' => 'download'));
	?>
</div>
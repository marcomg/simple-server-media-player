<?php
/**
 * Topbar.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<nav id="topbar" class="navbar navbar-default navbar-fixed-top container" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only"><?php echo __('Toggle navigation'); ?></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">SSMP</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<?php 
			$list = $this->Html->li($this->Html->link(__('Home'), '/', array('icon' => 'home')));

			if(!empty($all_sources['audio']))
				$html = $this->Html->linkDropdown(__('Audio'), array('icon' => 'music')).$this->Html->dropdown(array(
					$this->Html->link(__('Sources'), array('controller' => 'browser', 'action' => 'sources', 'type' => 'audio'), array('icon' => 'folder')),
					$this->Html->link(__('Playlists'), array('controller' => 'playlists', 'action' => 'index', 'type' => 'audio'), array('icon' => 'tasks'))
				));
			else
				$html = $this->Html->link(__('Audio'), '#', array('class' => 'disabled', 'icon' => 'music'));
			$list .= $this->Html->li($html, array('class' => 'dropdown'));

			if(!empty($all_sources['image']))
				$html = $this->Html->link(__('Images'), array('controller' => 'browser', 'action' => 'sources', 'type' => 'image'), array('icon' => 'camera-retro'));
			else
				$html = $this->Html->link(__('Images'), '#', array('class' => 'disabled', 'icon' => 'camera-retro'));
			$list .= $this->Html->li($html);

			if(!empty($all_sources['video']))
				$html = $this->Html->linkDropdown(__('Video'), array('icon' => 'film')).$this->Html->dropdown(array(
					$this->Html->link(__('Sources'), array('controller' => 'browser', 'action' => 'sources', 'type' => 'video'), array('icon' => 'folder')),
					$this->Html->link(__('Playlists'), array('controller' => 'playlists', 'action' => 'index', 'type' => 'video'), array('icon' => 'tasks'))
				));
			else
				$html = $this->Html->link(__('Video'), '#', array('class' => 'disabled', 'icon' => 'film'));
			$list .= $this->Html->li($html, array('class' => 'dropdown'));

			$list .= $this->Html->li($this->Html->link(__('Search'), array('controller' => 'browser', 'action' => 'search'), array('icon' => 'search')));

			echo $this->Html->tag('ul', $list, array('class' => 'nav navbar-nav'));
			
			$html = $this->Html->linkDropdown(__('Tools'), array('icon' => 'wrench'));
			$html .= $this->Html->dropdown(array(
				$this->Html->link(__('About'), array('controller' => 'pages', 'action' => 'view', 'about'), array('icon' => 'info-circle')),
				$this->Html->link(__('Shortcuts'), array('controller' => 'pages', 'action' => 'view', 'shortcuts'), array('icon' => 'keyboard-o')),
				$this->Html->link(__('Configuration'), array('controller' => 'tools', 'action' => 'configuration'), array('icon' => 'gears')),
				$this->Html->link(__('Manage sources'), array('controller' => 'tools', 'action' => 'sources'), array('icon' => 'folder')),
				$this->Html->link(__('System check up'), array('controller' => 'tools', 'action' => 'checkup'), array('icon' => 'stethoscope')),
				$this->Html->link(sprintf('%s/%s', __('Cache'), __('Thumbs')), array('controller' => 'tools', 'action' => 'cache'), array('icon' => 'trash-o')),
				$this->Html->link(__('Check for updates'), array('controller' => 'tools', 'action' => 'updates'), array('icon' => 'refresh'))
			));
			$list = $this->Html->li($html, array('class' => 'dropdown'));

			echo $this->Html->tag('ul', $list, array('class' => 'nav navbar-nav  navbar-right'));
		?>
	</div>
</nav>
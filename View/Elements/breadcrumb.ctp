<?php
/**
 * Breadcrumb.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<?php
	//All path elements
	$paths = array_filter(am(explode(DS, $data['path']), $data['filename']));
	
	$previous = array();
	
	if(!empty($paths)) {
		foreach($paths as $path) {
			//If this is the last path element
			if($path === end($paths)) {
				$this->Html->addCrumb($path);
				break;
			}
			//Adds the current path to the previous
			$previous[] = $path;
			
			$this->Html->addCrumb($path, am(array('controller' => 'browser', 'action' => 'browse', 'type' => $data['type'], 'id' => $data['id']), $previous));
		}
	}

	echo $this->Html->getCrumbList(array('class' => 'no-wrap'), array('icon' => 'home', 'url' => array('controller' => 'browser', 'action' => 'browse', $data['type'], $data['id'])));
?>
<?php
/**
 * Form to add media files to a playlist.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<?php
	//Gets playlists
	$playlists = $this->requestAction(array('controller' => 'playlists', 'action' => 'index', $data['type']));
	//Link to add a playlist
	$link = $this->Html->link(__('create'), array('controller' => 'playlists', 'action' => 'add', $data['type']));

	$html = $this->Form->createInline(FALSE);
	
	if(!empty($playlists)) {
		$html .= $this->Form->input('type', array(
			'empty'		=> sprintf('-- %s --', __('Select a playlist')),
			'id'		=> 'playlist-id',
			'onchange'	=> 'showAddToPlaylistBtn()',
			'options'	=> $playlists
		));
		$html .= $this->Html->div('text-muted', __('Otherwise, you can %s a new playlist', $link));
	}
	else
		$html .= $this->Html->div('text-muted', __('In order to add media files to a playlist, you need to %s a playlist', $link));
	
	$html .= $this->Form->end();

	echo $this->Html->div('no-display', $html, array('id' => 'add-to-playlist'));
?>
<?php
/**
 * jPlayer for audio playlists.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<?php
	$this->Html->js('jquery.jplayer.min');
	$this->Html->js('jplayer.playlist.min');
	$this->Html->css('jplayer-for-ssmp.min');
?>

<?php if(!empty($files)): ?>
	<?php echo $this->Html->scriptStart(); ?>
		$(function() {
			var jplayer = $("#jquery_jplayer_1");
			var myPlaylist = new jPlayerPlaylist(
				{jPlayer: jplayer, cssSelectorAncestor: "#jp_container_1"}, 
				[
					<?php foreach($files as $file): ?>
						{
							title: "<?php echo $file['filename']; ?>",
							<?php echo $this->Jplayer->getFormat($file['url']); ?>: "<?php echo $file['url']; ?>",
							<?php if(!empty($file['albumArt'])): ?>
								poster: "<?php echo $this->Html->thumbUrl($file['albumArt'], array('side' => '200')); ?>"
							<?php endif; ?>
						},
					<?php endforeach; ?>
				], 
				{
					<?php if($config['player']['shortcuts']): ?>keyEnabled: true,<?php endif; ?>
					playlistOptions: {
						autoPlay: true,
						<?php if($config['player']['autoplay']): ?>autoPlay: true,<?php endif; ?>
						enableRemoveControls: true
					},
					<?php if($config['player']['preload']): ?>preload: "auto",<?php endif; ?>
					size: { cssClass: "", height: "auto", width: "400px" },
					smoothPlayBar: true,
					solution: "html",
					supplied: "oga, mp3, wav",
					<?php if(!$isMobile): ?>volume: "<?php echo $config['player']['volume']; ?>"<?php endif; ?>
				}
			);
			<?php if(!empty($file['albumArt'])): ?>
				//On click on the poster, play if it's paused or vice versa
				$('img', jplayer).click(function() {
					if(jplayer.data().jPlayer.status.paused)
						jplayer.jPlayer('play');
					else
						jplayer.jPlayer('pause');
				});
			<?php endif; ?>
		 });
	<?php echo $this->Html->scriptEnd(); ?>

	<div>
		<div id="jp_container_1" class="jp-container jp-audio">
			<div id="jquery_jplayer_1" class="jp-jplayer"></div>
			<div class="jp-type-single">
				<div class="jp-gui jp-interface">			
					<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
					<div class="jp-controls">
						<a href="javascript:;" class="jp-button jp-play" tabindex="1"><?php echo $this->Html->icon('play'); ?></a>
						<a href="javascript:;" class="jp-button jp-pause" tabindex="1"><?php echo $this->Html->icon('pause'); ?></a>
						<div class="jp-timers"><span class="jp-current-time"></span> / <span class="jp-duration"></span></div>
						<?php if(!$isMobile): ?>
							<div class="pull-right">
								<a href="javascript:;" class="jp-button jp-mute" tabindex="1"><?php echo $this->Html->icon('volume-off'); ?></a>
								<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
								<a href="javascript:;" class="jp-button jp-volume-max" tabindex="1"><?php echo $this->Html->icon('volume-up'); ?></a>
							</div>
						<?php endif; ?>
						<div class="jp-no-solution"><?php echo __('Your browser can\'t play this media file, probably because there aren\'t necessary codecs or because it doesn\'t support HTML5 audio/video elements'); ?></div>
					</div>
				</div>
				<div class="jp-playlist"><ul><li></li></ul></div>
			</div>
		</div>
	</div>
<?php endif; ?>
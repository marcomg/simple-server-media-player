<?php
/**
 * jPlayer for video files.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<?php
	$this->Html->js('jquery.jplayer.min');
	$this->Html->css('jplayer-for-ssmp.min');
	//Sets the file format for jPlayer
	$format = $this->Jplayer->getFormat($url);
?>

<?php echo $this->Html->cssStart(); ?>
	<style type="text/css">
		.jp-container.jp-video video {
			height: auto !important;
			width: <?php echo $config['video']['width']; ?> !important;
		}
	</style>
<?php echo $this->Html->cssEnd(); ?>

<?php echo $this->Html->scriptStart(); ?>
	$(function() {
		var jplayer = $("#jquery_jplayer_1");
		jplayer.jPlayer({
			ready: function() {
				$(this).jPlayer("setMedia", { <?php echo $format; ?>: "<?php echo $url; ?>" });
				<?php if($config['player']['autoplay']): ?>$(this).jPlayer("play");<?php endif; ?>
				//On click on the video, play if it's paused or vice versa
				$('video', jplayer).click(function() {
					if(jplayer.data().jPlayer.status.paused)
						jplayer.jPlayer('play');
					else
						jplayer.jPlayer('pause');
				});
			},
			<?php if($config['player']['shortcuts']): ?>keyEnabled: true,<?php endif; ?>
			<?php if($config['player']['preload']): ?>preload: "auto",<?php endif; ?>
			size: { cssClass: "", height: "auto", width: "<?php echo $config['video']['width']; ?>" },
			smoothPlayBar: true,
			solution: "html",
			supplied: "<?php echo $format; ?>",
			<?php if(!$isMobile): ?>volume: "<?php echo $config['player']['volume']; ?>"<?php endif; ?>
		});
	});
<?php echo $this->Html->scriptEnd(); ?>

<div>
	<div id="jp_container_1" class="jp-container jp-video">
		<div id="jquery_jplayer_1" class="jp-jplayer"></div>
		<div class="jp-type-single">
			<div class="jp-gui jp-interface">			
				<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
				<div class="jp-controls">
					<a href="javascript:;" class="jp-button jp-play" tabindex="1"><?php echo $this->Html->icon('play'); ?></a>
					<a href="javascript:;" class="jp-button jp-pause" tabindex="1"><?php echo $this->Html->icon('pause'); ?></a>
					<div class="jp-timers"><span class="jp-current-time"></span> / <span class="jp-duration"></span></div>
					<div class='pull-right'>
						<a href="javascript:;" class="jp-button jp-full-screen" tabindex="1"><?php echo $this->Html->icon('arrows-alt'); ?></a>
						<a href="javascript:;" class="jp-button jp-restore-screen" tabindex="1"><?php echo $this->Html->icon('compress'); ?></a>
					</div>
					<?php if(!$isMobile): ?>
						<div class="pull-right">
							<a href="javascript:;" class="jp-button jp-mute" tabindex="1"><?php echo $this->Html->icon('volume-off'); ?></a>
							<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
							<a href="javascript:;" class="jp-button jp-volume-max" tabindex="1"><?php echo $this->Html->icon('volume-up'); ?></a>
						</div>
					<?php endif; ?>
					<div class="jp-no-solution"><?php echo __('Your browser can\'t play this media file, probably because there aren\'t necessary codecs or because it doesn\'t support HTML5 audio/video elements'); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
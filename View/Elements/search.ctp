<?php
/**
 * Search form.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Elements
 */
?>

<div id="search-form">
	<?php echo $this->Form->create('Media', am(array('inline' => TRUE, 'url' => array('controller' => 'browser', 'action' => 'search')), empty($options) ? array() : $options)); ?>
		<fieldset>
			<?php 
				echo $this->Form->input('p', array(
					'label'			=> __('Pattern'), 
					'placeholder'	=> __('What are you searching for?'),
					'tip'			=> __('The search is case-insensitive and you don\'t have to specify the file extension, even if you\'re using a regex')
				));
				echo $this->Form->input('t', array(
					'empty'		=> __('I\'m searching for...'),
					'label'		=> __('Media type'),
					'options'	=> array('audio' => __('Audio'), 'image' => __('Images'), 'video' => __('Videos'))
				));
				echo $this->Form->input('r', array('hiddenField' => FALSE, 'label' => sprintf('%s?', __('Regex')), 'type' => 'checkbox'));
				echo $this->Form->submit(__('Search'), array('class' => 'btn-info', 'icon' => 'search'));
			?>
		</fieldset>
	<?php echo $this->Form->end(); ?>
</div>
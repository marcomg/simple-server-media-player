<?php
/**
 * Add a new source.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php echo $this->Html->tag('h3', __('Add source').$this->Html->tag('small', __('Add a new source'))); ?>
<?php echo $this->Form->create('Source'); ?>
	<fieldset>
		<?php
			echo $this->Form->input('path', array('label' => __('Source path')));
			echo $this->Form->input('type', array(
				'empty'		=> __('Select a type of source'),
				'label'		=> __('Source type'),
				'options'	=> array('audio' => __('Audio'), 'image' => __('Images'), 'video' => __('Videos'))
			));
		?>
	</fieldset>
<?php echo $this->Form->end(array('label' => __('Add source'), 'icon' => 'plus')); ?>
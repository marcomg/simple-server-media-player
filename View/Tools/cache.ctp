<?php
/**
 * Provides information and allows you to clear the cache and thumbnails.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php 
	echo $this->Html->tag('h3', sprintf('%s/%s', __('Cache'), __('Thumbs')).$this->Html->tag('small', __('Manage cache and thumbs')));

    echo $this->Html->tag('h4', __('Cache'));
    if($cacheStatus) {
        echo $this->Html->para(NULL, sprintf('%s %s', __('The cache is enabled.'), __('Cache size: %s.', $cacheSize)));
        echo $this->Html->para(NULL, __('Note: you should not need to clear the cache, unless you have not changed the configuration or after an upgrade of %s.', 'SSMP'));
        echo $this->Form->postLink(__('Clear the cache'), array('action' => 'clearCache'), array('class' => 'btn-primary margin20', 'icon' => 'trash-o'));
    }
    else
        echo $this->Html->para('text-danger', __('The cache is disabled.'));

    echo $this->Html->tag('h4', __('Thumbs'));
    echo $this->Html->para(NULL, __('Thumbs size: %s.', $thumbsSize));
    echo $this->Html->para(NULL, __('Note: you should not need to clear thumbnails and that this will slow down the images loading the first time that are displayed. You should clear thumbnails only when they have reached a large size or when many images are no longer used.'));
    echo $this->Form->postLink(__('Clear thumbs'), array('action' => 'clearThumbs'), array('class' => 'btn-primary', 'icon' => 'trash-o'));
?>
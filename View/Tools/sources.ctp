<?php
/**
 * Manages sources.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php 
	echo $this->Html->tag('h3', __('Sources').$this->Html->tag('small', __('Manage sources')));
	echo $this->Html->para(NULL, __('You can add %s.', $this->Html->link(__('new sources'), array('action' => 'add_source'))));
	
	if(!empty($sources)) {
		$html = NULL;
		foreach($sources as $type => $sources) {			
			$html .= $this->Html->tag('h4', $type == 'audio' ? __('Audio sources') : ($type == 'image' ? __('Image sources') : __('Video sources')));
			
			foreach($sources as $id => $source) {
				$link = $this->Form->postLink(NULL, array('action' => 'delete_source', $type, $id), array('class' => 'no-display pull-right', 'icon' => 'times'), __('Are you sure you want to delete this source?'));
				
				switch($source['check']) {
					case 'valid':
						$html .= $this->Html->div('source bg-success text-success margin10 padding10', __('%s is a valid source', $this->Html->strong($source['source'])).$link, array('icon' => 'check-circle'));
						break;
					case 'not_exists':
						$html .= $this->Html->div('source bg-danger text-danger margin10 padding10', __('%s doesn\'t exist', $this->Html->strong($source['source'])).$link, array('icon' => 'times-circle'));
						break;
					case 'not_readable':
						$html .= $this->Html->div('source bg-danger text-danger margin10 padding10', __('%s exists, but is not readable', $this->Html->strong($source['source'])).$link, array('icon' => 'times-circle'));
						break;
					case 'not_dir':
						$html .= $this->Html->div('source bg-danger text-danger margin10 padding10', __('%s is not a directory', $this->Html->strong($source['source'])).$link, array('icon' => 'times-circle'));
						break;
					case 'is_empty':
						$html .= $this->Html->div('source bg-warning text-warning margin10 padding10', __('%s is a valid source, but it\'s empty', $this->Html->strong($source['source'])).$link, array('icon' => 'exclamation-circle'));
						break;
				}
			}
		}
		
		echo $this->Html->div(NULL, $html, array('id' => 'sources'));
	}
?>
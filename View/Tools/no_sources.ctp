<?php
/**
 * No sources page.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php	
	echo $this->Html->tag('h3', NULL, array('class' => 'no-margin text-center', 'id' => 'sad-smile', 'icon' => 'frown-o'));
	echo $this->Html->tag('h4', __('What\'s wrong?'), array('class' => 'text-center'));
	
	echo $this->Html->para('text-center', __('It seems that there are no configured sources, or that all sources are invalid!'));
	echo $this->Html->para('text-center', __('First, make sure the file %s exists and is readable.', $this->Html->code($file)));
	echo $this->Html->para('text-center', __('Then edit this file, adding your sources. If you have any problems, please refer to %s.',
		$this->Html->link(__('our wiki'), '//git.novatlantis.it/simple-server-media-player/wiki/Configure%20SSMP', array('target' => '_blank'))));
	echo $this->Html->para('text-center', __('Otherwise you can use the interface for the %s and %s.',
		$this->Html->link(__('management of the sources'), array('action' => 'sources')),
		$this->Html->link(__('add a new source'), array('action' => 'add_source'))));
	
	if(!empty($referer))
		echo $this->Html->div('text-center', $this->Html->button(__('Try again'), $referer, array('class' => 'btn-primary', 'icon' => 'refresh')));
?>
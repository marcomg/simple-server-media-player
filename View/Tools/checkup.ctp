<?php
/**
 * Performs a system check-up.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>
	
<?php
	echo $this->Html->tag('h3', __('Checkup').$this->Html->tag('small', __('System checkup')));
	echo $this->Html->para('bg-info text-info padding10', __('SSMP version: %s', $ssmpVersion));
	echo $this->Html->para('bg-info text-info padding10', __('CakePHP version: %s', $cakeVersion));
	echo $this->Html->para('bg-info text-info padding10', __('MeTools version: %s', $metoolsVersion));
	
	echo $this->Html->tag('h4', 'Apache');
	if($rewrite)
		echo $this->Html->para('bg-success text-success padding10', __('The %s module is enabled', $this->Html->strong('Rewrite')), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The %s module is not enabled', $this->Html->strong('Rewrite')), array('icon' => 'times-circle'));
	
	if($expires)
		echo $this->Html->para('bg-success text-success padding10', __('The %s module is enabled', $this->Html->strong('Expires')), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The %s module is not enabled', $this->Html->strong('Expires')), array('icon' => 'times-circle'));
	
	echo $this->Html->tag('h4', 'PHP');
	if($phpVersion)
		echo $this->Html->para('bg-success text-success padding10', __('The PHP version is at least %s', $this->Html->strong($phpRequired)), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The PHP version is less than %s', $this->Html->strong($phpRequired)), array('icon' => 'times-circle'));
	
	if($gd)
		echo $this->Html->para('bg-success text-success padding10', __('The %s extension is enabled', $this->Html->strong('GD')), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The %s extension is not enabled', $this->Html->strong('GD')), array('icon' => 'times-circle'));
	
	if($exif)
		echo $this->Html->para('bg-success text-success padding10', __('The %s extension is enabled', $this->Html->strong('Exif')), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The %s extension is not enabled', $this->Html->strong('Exif')), array('icon' => 'times-circle'));
	
	if($fileinfo)
		echo $this->Html->para('bg-success text-success padding10', __('The %s extension is enabled', $this->Html->strong('FileInfo')), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The %s extension is not enabled', $this->Html->strong('FileInfo')), array('icon' => 'times-circle'));
	
	echo $this->Html->tag('h4', __('Temporary directory'));
	if($tmp)
		echo $this->Html->para('bg-success text-success padding10', __('The temporary directory is readable and writable'), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The temporary directory is not readable or writable'), array('icon' => 'times-circle'));
	
	if($cacheStatus)
		echo $this->Html->para('bg-success text-success padding10', __('The cache is enabled'), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The cache is disabled'), array('icon' => 'times-circle'));
	
	if($cache)
		echo $this->Html->para('bg-success text-success padding10', __('The cache is readable and writable'), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The cache is not readable or writable'), array('icon' => 'times-circle'));
	
	if($playlists)
		echo $this->Html->para('bg-success text-success padding10', __('The playlist indexes are readable and writable'), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The playlist indexes are not readable or writable'), array('icon' => 'times-circle'));
	
	if($thumbs)
		echo $this->Html->para('bg-success text-success padding10', __('The thumbs directory is readable and writable'), array('icon' => 'check-circle'));
	else
		echo $this->Html->para('bg-danger text-danger padding10', __('The thumbs directory is not readable or writable'), array('icon' => 'times-circle'));
?>	
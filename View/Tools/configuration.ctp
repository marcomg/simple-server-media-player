<?php
/**
 * Set configuration.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php echo $this->Html->tag('h3', __('Configuration').$this->Html->tag('small', __('SSMP configuration'))); ?>
<?php echo $this->Form->create('Configuration'); ?>
	<fieldset>
		<?php
			echo $this->Form->legend(__('App options'));
			echo $this->Form->input('app_language', array(
				'label'		=> __('Language'),
				'options'	=> array('auto' => __('Auto'), 'eng' => __('English'), 'ita' => __('Italian')),
				'tip'		=> __('With "auto" value, SSMP will use the browser language, if this is available, '
					.'otherwise it will use english'),
			));
			echo $this->Form->input('app_logo', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Show logo'),
				'tip'			=> __('Note that on mobile devices, such as mobile phones or small tablets, however '
					.'the logo will not be shown'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			
			echo $this->Form->legend(__('Audio options'));
			echo $this->Form->input('audio_albumArt', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Show album art'),
				'tip'			=> __('Show album art for audio files'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('audio_id3Tags', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Show ID3 tags'),
				'tip'			=> __('Show ID3 tags for audio files'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			
			echo $this->Form->legend(__('Image options'));
			echo $this->Form->input('image_exifData', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Show EXIF data'),
				'tip'			=> __('Show EXIF data for image files'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
					
			echo $this->Form->legend(__('Video options'));
			echo $this->Form->input('video_id3Tags', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Show ID3 tags'),
				'tip'			=> __('Show ID3 tags for video files'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('video_width', array(
				'label'	=> __('Video width'),
				'tip'	=> __('With "auto" value, will be used the original width of the video. Otherwise, you can specify '
					.'an integer (greater than 240) that indicates the video width in pixels. Note that in each case, the '
					.'video may not have a width greater than that of the window')
			));
			
			echo $this->Form->legend(__('Player options'));
			echo $this->Form->input('player_autoplay', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Enable autoplay'),
				'tip'			=> __('Autoplay media files. Note that this setting will be also applied to playlists'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('player_nativeOnMobile', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Uses the native player on mobile devices'),
				'tip'			=> __('Will be forced to use the native player on all mobile devices. It is recommended, '
					.'because the player interacts better with the native controls of mobile devices. If this option is '
					.'disabled will be used instead of the player shown to the "type" in each case'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('player_preload', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Preload files'),
				'tip'			=> __('SSMP can preload the media file when the page loads. Note that some mobile devices '
					.'prevent preloading, so that the data traffic is not consumed unnecessarily'),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('player_shortcuts', array(
				'hiddenField'	=> 'false',
				'label'			=> __('Enable shortcuts'),
				'tip'			=> __('Using jPlayer as media player, you can use keyboard shortcuts to control the player. '
					.'Look at the keyboard %s', $this->Html->link(__('shortcuts'), array('controller' => 'pages', 'action' => 'view', 'shortcuts'))),
				'type'			=> 'checkbox',
				'value'			=> 'true',
			));
			echo $this->Form->input('player_type', array(
				'label'		=> __('Player type'),
				'options'	=> array('jplayer' => 'jPlayer', 'native' => __('Native')),
				'tip'		=> __('jPlayer offers the same graphic rendering and the same functions on every browser and it '
					.'also can verify that your browser is able to play a media format. jPlayer is strongly recommended. '
					.'Also, <strong>if you wanna use playlists</strong> you have to use jPlayer')
			));
			echo $this->Form->input('player_volume', array(
				'label'	=> __('Default volume'),
				'min'	=> '0',
				'max'	=> '100',
				'tip'	=> __('Using jPlayer as media player, you can set the default volume. This must be an integer between '
					.'"0" and "100". This will not work on some mobile devices, where the volume control is left to the device'),
				'type'	=> 'number'
			));
			
			echo $this->Form->legend(__('Security options'));
			echo $this->Form->input('security_ipAllowed', array(
				'label'	=> __('IP addresses allowed'),
				'tip'	=> __('Addresses must be separated by a comma (optionally by a comma and a space). You can use the '
					.'asterisk (*) as a wildcard. With an empty value, access is granted to any ip addresses (no limitation). '
					.'See %s before using this option', $this->Html->link(__('our wiki'), '//git.novatlantis.it/simple-server-media-player/wiki/Allow%20access%20based%20on%20ip', array('target' => 'blank')))
			));
			echo $this->Form->input('security_ipDenied', array(
				'label'	=> __('IP addresses denied'),
				'tip'	=> __('Addresses must be separated by a comma (optionally by a comma and a space). You can use the '
					.'asterisk (*) as a wildcard. With an empty value, access is granted to any ip addresses (no limitation). '
					.'See %s before using this option', $this->Html->link(__('our wiki'), '//git.novatlantis.it/simple-server-media-player/wiki/Allow%20access%20based%20on%20ip', array('target' => 'blank')))
			));
			echo $this->Form->input('security_searchInterval', array(
				'label'	=> __('Search interval'),
				'tip'	=> __('Time interval required to perform a new search, in seconds. With "0" or an empty value, no '
					.'time interval will be required (no limit)')
			));;
		?>
	</fieldset>
<?php echo $this->Form->end(array('label' => __('Save options'), 'icon' => 'check')); ?>
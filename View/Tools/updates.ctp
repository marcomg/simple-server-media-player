<?php
/**
 * Checks for updates.
 *
 * This file is part of SSMP.
 *
 * SSMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * SSMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MeTools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author		Mirko Pagliai <mirko.pagliai@gmail.com>
 * @copyright	Copyright (c) 2014, Mirko Pagliai for Nova Atlantis Ltd
 * @license		http://www.gnu.org/licenses/agpl.txt AGPL License
 * @link		http://git.novatlantis.it/simple-server-media-player Nova Atlantis Ltd
 * @package		Ssmp\View\Tools
 */
?>

<?php 
	echo $this->Html->tag('h3', __('Updates').$this->Html->tag('small', __('%s updated', 'SSMP')));
	
	if(!empty($latest)) {
		//If a new version is available
		if($newVersionAvailable)
			echo $this->Html->para(NULL, __('It\'s available %s %s. You can get more information and download the new version from %s.', $this->Html->strong('SSMP'), $this->Html->strong($latest['version']), $this->Html->link(__('here'), $latest['link'], array('target' => '_blank'))));
		//Else, if a new version is not available
		else
			echo $this->Html->para(NULL, __('There are no updates, %s is updated to the latest version.', 'SSMP'));
	}
	else
		echo $this->Html->para(NULL, __('Impossible to check for updates. Please, look on %s', $this->Html->link(__('our website'), 'http://ssmp.novatlantis.it', array('target' => '_blank'))));
?>